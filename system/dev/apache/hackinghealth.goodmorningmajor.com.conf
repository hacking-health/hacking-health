<VirtualHost *:8080>

    ServerAdmin thomas@goodmorningmajor.com
    DocumentRoot /var/www/hackinghealth.goodmorningmajor.com/web
    ServerName hackinghealth.goodmorningmajor.com

    CustomLog /var/log/apache2/goodmorningmajor.com/hackinghealth/access.log combined
    ErrorLog /var/log/apache2/goodmorningmajor.com/hackinghealth/error.log

    <Directory "/var/www/hackinghealth.goodmorningmajor.com/web/app/uploads">

        Options None
        Options +FollowSymLinks

        AllowOverride None
        DirectoryIndex Off

        # On interdit l'exécution du php dans le répertoire des uploads
        php_admin_flag engine off
        
        # On interdit le téléchargement des fichiers PHP non interprétés
        RewriteEngine On
        RewriteRule \.php$ - [F,L]
    </Directory>

    <Directory "/var/www/hackinghealth.goodmorningmajor.com/web">

        Options None
        Options +FollowSymLinks

        # Désactivation des .htaccess sur l'ensemble du site
        # Les configurations de Wordpress doivent être effectuées depuis ce fichier de virtualhost
        AllowOverride None

        SetOutputFilter DEFLATE

        php_flag display_errors Off
        php_value date.timezone "Europe/Paris"

        ExpiresActive On
        ExpiresDefault "access plus 1 day"

        <FilesMatch "\.(gif|jpg|png|js|css)$">
            ExpiresDefault "access plus 1 month"
        </FilesMatch>

        Require all granted

        ServerSignature Off
        LimitRequestBody 10240000

        <files .htaccess>
            order allow,deny
            deny from all
        </files>
        
        <files wp-config.php>
            order allow,deny
            deny from all
        </files>

        # BEGIN WordPress
        <IfModule mod_rewrite.c>
            RewriteEngine On

            RewriteBase /
            RewriteRule ^index\.php$ - [L]
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteRule . /index.php [L]
        </IfModule>

        # END WordPress

        # PART I: QUERY STRINGS 
        <ifmodule mod_rewrite.c>
            RewriteCond %{QUERY_STRING} ftp\:   [NC,OR]
            RewriteCond %{QUERY_STRING} http\:  [NC,OR]
            RewriteCond %{QUERY_STRING} https\: [NC]
            RewriteRule .* -                    [F,L]
        </ifmodule>

        # PART II: USER AGENTS
        SetEnvIfNoCase User-Agent "Jakarta Commons" keep_out
        SetEnvIfNoCase User-Agent "Y!OASIS/TEST"    keep_out
        SetEnvIfNoCase User-Agent "libwww-perl"     keep_out
        SetEnvIfNoCase User-Agent "MOT-MPx220"      keep_out
        SetEnvIfNoCase User-Agent "MJ12bot"         keep_out
        SetEnvIfNoCase User-Agent "Nutch"           keep_out
        SetEnvIfNoCase User-Agent "cr4nk"           keep_out
        <Limit GET POST PUT>
            order allow,deny
            allow from all
            deny from env=keep_out
        </Limit>

        # PART III: IP ADDRESSES
        <Limit GET POST PUT>
           order allow,deny
           allow from all
           deny from 75.126.85.215  "# blacklist candidate 2008-01-02 = admin-ajax.php attack "
           deny from 128.111.48.138 "# blacklist candidate 2008-02-10 = cryptic character strings "
           deny from 87.248.163.54  "# blacklist candidate 2008-03-09 = block administrative attacks "
           deny from 84.122.143.99  "# blacklist candidate 2008-04-27 = block clam store loser "
        </Limit>

        ######### END PERISHABLE PRESS 3G BLACKLIST

    </Directory>

</VirtualHost>
