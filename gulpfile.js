var gulp = require('gulp'),
    php = require('gulp-connect-php'),
    browserSync = require('browser-sync'),
    less = require('gulp-less-sourcemap'),
    path = require('path');

gulp.task('reload', function () {
  browserSync.reload();
});

gulp.task('less', function () {
  gulp.src('web/app/themes/hacking-health/less/theme/theme-global.less')
    .pipe(less({
        sourceMap: {
            sourceMapRootpath: '../less/theme' // Optional absolute or relative path to your LESS files
        }
    }))
    .on('error', function(error) {
      console.log('err : '+error.message);
    })
    .pipe(gulp.dest('web/app/themes/hacking-health/css'));
});

gulp.task('connect-sync', function() {
  php.server({base: 'web'}, function() {
    browserSync({
      baseDir: 'web',
      proxy: '127.0.0.1:8000',
      open: false,
    });
  });
});

gulp.task('default', ['less','connect-sync'], function () {
  gulp.watch(['web/*.php'], ['reload']);
  gulp.watch(['web/app/themes/hacking-health/less/theme/**/*.less'], ['less','reload']);
});

var sftp = require('gulp-sftp');
var changed = require('gulp-changed');
var SSH = require('gulp-ssh');
var ssh = new SSH();
var git = require('gulp-git');

var user = '40880';
var sshKey = '/Users/sebmade/.ssh/hhgandi_rsa';
var vhost = 'hackinghealth.ca';

gulp.task('sftp', function() {
  return gulp.src('web/app/uploads/**/*')
        .pipe(changed('dist'))
        .pipe(gulp.dest('dist'))
        .pipe(sftp({
          host: 'sftp.sd6.gpaas.net',
          user: user,
          key: sshKey,
          remotePath: 'vhosts/'+vhost+'/web/app/uploads/'
        }));
});

gulp.task('deploy', ['sftp'], function () {
  git.push('gandi', 'master', function(err) {
    if (err) {
      console.log(err);
    } else {
      var s = ssh.connect({host: 'git.sd6.gpaas.net', user: user, privateKey: require('fs').readFileSync(sshKey)})
      s.exec('deploy '+vhost+'.git')
      .pipe(gulp.dest('logs'))
      .on('end', function() {
        s.close();
      });
    }
  });
});

var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');

gulp.task('iconfont', function(){
  return gulp.src(['assets/SVG/*.svg'])
    .pipe(iconfont({ fontName: 'icomoon', appendUnicode: true, normalize: true, formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'] }))
    .on('glyphs', function(glyphs, options) {
      gulp.src('assets/theme-fonts.less')
        .pipe(consolidate('lodash', {glyphs: glyphs}))
        .pipe(gulp.dest('web/app/themes/hacking-health/less/theme/'));
    })
    .pipe(gulp.dest('web/app/themes/hacking-health/fonts/'));
});

var exec = require('child_process').exec;
var localDbFile = 'localdb.sql';

gulp.task('dumplocaldb', function(cb) {
  exec('mysqldump -u root hackinghealth -h 127.0.0.1 > '+localDbFile, function(err) {
    cb(err);
  })
});

var replace = require('gulp-replace');

gulp.task('push2server', ['dumplocaldb'], function() {
  return gulp.src(localDbFile)
      .pipe(replace('127.0.0.1:3000','hackinghealth.ca'))
      .pipe(replace('127.0.0.1:8000','hackinghealth.ca'))
      .pipe(sftp({
        host: 'sftp.dc0.gpaas.net',
        user: user,
        key: sshKey,
        remotePath: 'lamp0/tmp/'
      }));
});

gulp.task('import2DbProd', ['push2server'], function (cb) {
  var s = ssh.connect({host: 'git.dc0.gpaas.net', user: user, privateKey: require('fs').readFileSync(sshKey)})
  s.exec([/*'mysqldump -u root -phackinghealth hackinghealth > tmp/hhdb-backup.sql', */'mysql -u root -P hackinghealth hackinghealth < '+localDbFile])
  .pipe(gulp.dest('logs'))
  .on('end', function() {
    s.close();
    cb();
  });
});

var scpClient = require('scp2');

gulp.task('scp', [], function(cp) {
  scpClient.scp('web/app/uploads', {
      "host": "62.210.177.180",
      "port": "22",
      "username": "hackinghealth",
      "password": "r8oaYxCMRxMho2YpsTrEMzuNJ9F7xD",
      "path": "/home/hackinghealth/websites/hackinghealth.goodmorningmajor.com/web/app/uploads",
      "agent": process.env["SSH_AUTH_SOCK"],
      "agentForward": true
  }, cb)
});
