<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

/**
 * Modifying TinyMCE editor to remove unused items.
 *
 * @since GMM's Bootstrap 1.0
 * @see http://www.wpexplorer.com/wordpress-tinymce-tweaks/
 */
function gmm_styles_dropdown($settings) {

	// Create array of new styles
	$new_styles = array(
		array(
			'title' => __('Boutons', 'hacking'),
			'items' => array(
				array(
					'title'		=> __('Bouton bloc', 'hacking'),
					'selector'	=> 'a',
					'classes'	=> 'btn btn-tertiary btn-next btn-block',
				),
			),
		),
		array(
			'title' => __('Retour à la ligne', 'hacking'),
			'items' => array(
				array(
					'title'		=> __('Forcer le retour à la ligne', 'hacking'),
					'selector'	=> 'div,p,ul,ol,blockquote,h1,h2,h3,h4,h5,h6',
					'classes'	=> 'clear',
				),
			),
		),
		array(
			'title' => __('Style de titre', 'hacking'),
			'items' => array(
				array(
					'title'		=> __('Style de titre H1', 'hacking'),
					'selector'	=> 'div,p,h1,h2,h3,h4,h5,h6',
					'classes'	=> 'h1',
				),
				array(
					'title'		=> __('Style de titre H2', 'hacking'),
					'selector'	=> 'div,p,h1,h2,h3,h4,h5,h6',
					'classes'	=> 'h2',
				),
				array(
					'title'		=> __('Style de titre H3', 'hacking'),
					'selector'	=> 'div,p,h1,h2,h3,h4,h5,h6',
					'classes'	=> 'h3',
				),
				array(
					'title'		=> __('Style de titre H4', 'hacking'),
					'selector'	=> 'div,p,h1,h2,h3,h4,h5,h6',
					'classes'	=> 'h4',
				),
				array(
					'title'		=> __('Style de titre H5', 'hacking'),
					'selector'	=> 'div,p,h1,h2,h3,h4,h5,h6',
					'classes'	=> 'h5',
				),
				array(
					'title'		=> __('Style de titre des prix du hackhathon', 'hacking'),
					'selector'	=> 'div,p',
					'classes'	=> 'title',
				),
			),
		),
	);

	// Merge old & new styles
	$settings['style_formats_merge'] = true;

	// Add new styles
	$settings['style_formats'] = json_encode( $new_styles );
	
	// Return New Settings
	return $settings;
}
add_filter('tiny_mce_before_init', 'gmm_styles_dropdown' );

/**
 * Custom login error message. Secure bad password typing.
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_custom_login_errors( $text ) {
	$erreur = __("ERREUR", 'hacking');
	$wrong_user_pass = __("le mot de passe ou l'identifiant n'est pas le bon", 'hacking');
	$lost_password = __("Avez-vous perdu votre mot de passe ?", 'hacking');
	$message = "<strong>$erreur</strong> : $wrong_user_pass. <br><a href=\"/wp-login.php?action=lostpassword\">$lost_password</a>";
	
	return $message;
}
add_filter('login_errors', 'gmm_custom_login_errors');

/**
 * Hide view post in admin
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_posttype_admin_css() {
	global $post_type;
	$post_types = array(
	/* set post types */
		'slide',
	);
	
	if ( in_array($post_type, $post_types) ) {
		echo '<style type="text/css">#edit-slug-box, #post-preview, #view-post-btn{display: none;}</style>';
	}
}
add_action( 'admin_head-post-new.php', 'gmm_posttype_admin_css' );
add_action( 'admin_head-post.php', 'gmm_posttype_admin_css' );