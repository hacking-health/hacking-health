<?php
/**
 * Custom shortcodes for this theme.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

/**
 * Bootstrap shortcodes
 */
function gmm_row_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"class"	=> '',
	), $atts ) );

	$row_classes = trim($class) ? explode(' ', trim($class)) : array();
	array_unshift($row_classes, 'row');
	$row_classes = implode(' ', $row_classes);

	// Wrap content in row
	// Call do_shortcode to execute other shortcodes (like [col])
	$content = '<div class="'. $row_classes .'">'. do_shortcode( $content ) .'</div>';

	if ( is_page_template( 'templates/enseigne.php' ) ) {
		$content = '<div class="container">'. $content .'</div>';
	}

	return $content;
}
add_shortcode( 'row', 'gmm_row_shortcode' );

function gmm_col_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"xs"	    => '',
		"offset_xs" => '',
		"sm"	    => '',
		"offset_sm" => '',
		"md"	    => '',
		"offset_md" => '',
		"class"	    => '',
	), $atts ) );

	// Init and check bootstrap col CSS class
	$xs = (int)$xs;
	$sm = (int)$sm;
	$md = (int)$md;
	$offset_xs = (int)$offset_xs;
	$offset_sm = (int)$offset_sm;
	$offset_md = (int)$offset_md;

	$col_classes = trim($class) ? explode(' ', trim($class)) : array();

	if ( bootstrap_valid_col($xs) ) {
		$col_classes[] = 'col-xs-'. $xs;
		$col_classes[] = 'col-xs-offset-'. $offset_xs;
	}

	if ( bootstrap_valid_col($sm) ) {
		$col_classes[] = 'col-sm-'. $sm;
		$col_classes[] = 'col-sm-offset-'. $offset_sm;
	}

	if ( bootstrap_valid_col($md) ) {
		$col_classes[] = 'col-md-'. $md;
		$col_classes[] = 'col-md-offset-'. $offset_md;
	}

	$col_classes = implode(' ', $col_classes);

	// Wrap content in col
	$content = '<div class="'. $col_classes .'">'. do_shortcode( $content ) .'</div>';
	return $content;
}
add_shortcode( 'col', 'gmm_col_shortcode' );


/**
 * Check if is a valid Bootstrap col
 */
function bootstrap_valid_col($col) {
	return in_array($col, range(1, 12));
}

/**
 * Wrap content into banner
 */
function gmm_banner_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Banner colors definition
	$class = "primary";

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"class" => $class,
	), $atts ) );


	// Wrap content in the styled banner
	$banner_class = $class. '-banner';
	$content = '<div class="'. $banner_class .' banner relative entry-content">
					<div class="bottom-arrow"></div>
					<div class="container">'. do_shortcode( $content ) .'</div>
				</div>';
	return $content;
}
add_shortcode( 'banner', 'gmm_banner_shortcode' );

/**
 * Add a shortcode for the bloc info section in the content
 */
function gmm_image_banner_shortcode( $atts ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"title"	=> '',
		"title_color" => '',
		"baseline" => '',
		"baseline_color" => '',
	), $atts ) );

	$url = wp_get_attachment_url( get_post_thumbnail_id( get_the_id() ) );

	// Wrap content
	$img_banner =	'<div class="img-banner" style="background-image: url(' . $url . ');">
						<div class="container">
							<div class="text-center">
								<p class="title" style="color: ' . $title_color . ';">' . $title .'</p>
								<p class="baseline" style="color: ' . $baseline_color . ';">' . $baseline . '</p>
							</div>
						</div>
					</div>';
	return $img_banner;
}
add_shortcode( 'image-banner', 'gmm_image_banner_shortcode' );

/**
 * Wrap content into banner
 */
function gmm_template_parts_shortcode( $atts ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"path" => '',
	), $atts ) );

	ob_start();
	get_template_part( $path );
	$content = ob_get_contents();
	ob_end_clean();

	// Wrap content in the styled banner
	return $content;
}
add_shortcode( 'template-parts', 'gmm_template_parts_shortcode' );

/**
 * Well panel with icon
 */
function hh_icon_block_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"icon" => '',
		"title" => '',
		"description" => '',
		"cssbgcolor" => '',
		"page" => ''
	), $atts ) );

	$content = '<div class="hh-icon-block"><div>';
	if ($page != null) {
			$content .= '<a href="/index.php/'.$page.'" class="icon-'.$icon.' '.$cssbgcolor.'"></a>';
	} else {
		$content .= '<span class="icon-'.$icon.' '.$cssbgcolor.'"></span>';
	}
	$content .= '</div><h4>'.$title.'</h4><p>'.$description.'</p></div>';
	return $content;
}
add_shortcode( 'hh-icon-block', 'hh_icon_block_shortcode' );

/**
 * Well panel with image
 */
function hh_image_block_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"image" => '',
		"title" => '',
		"description" => '',
		"page" => ''
	), $atts ) );

	$content = '<div class="hh-image-block"><div>';
	if ($page != null) {
			$content .= '<a href="'.$page.'"><img src="/app/uploads/'.$image.'" class="img-responsive"></a>';
	} else {
		$content .= '<img src="/app/uploads/'.$image.'" class="img-responsive">';
	}
	$content .= '</div><h3 class="h5">'.$title.'</h3><p>'.$description.'</p></div>';
	return $content;
}
add_shortcode( 'hh-image-block', 'hh_image_block_shortcode' );

function hh_panel_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"class" => "panel-default",
	), $atts ) );

	$result = '<div class="panel '. $class .'">'. do_shortcode( $content ) .'</div>';
	return $result;
}
add_shortcode( 'hh-panel', 'hh_panel_shortcode' );

function hh_panel_content_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"icon" => '',
		"title" => '',
		"cssbgcolor" => ''
	), $atts ) );

	$result = '<div class="panel-heading">';
	if ($icon != null) {
		$result .= '<span class="panel-title-icon icon-'.$icon.' '.$cssbgcolor.'"></span>';
	}
	$result .= '<p>'.$title.'</p></div><div class="panel-body">'. do_shortcode( $content ) .'</div>';
	return $result;
}
add_shortcode( 'hh-panel-content', 'hh_panel_content_shortcode' );

function hh_person_item_shortcode( $atts ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"title"	=> '',
		"photo" => '2015/08/img.png',
		"function" => '',
		"description" => '',
		"content" => '',
		"twitter" => '',
		"linkedin" => ''
	), $atts ) );

	$result = '<div class="hh-item-person">';
	$result .= '<img class="img-responsive img-person" src="/app/uploads/'.$photo.'">';
	$result .= '<p class="function">'.$function.'</p>';
	$result .= '<h3 class="title">'.$title.'</h3>';
	$result .= '<span class="hr"></span>';
	$result .= '<p class="description">'.$description.'</p>';
	$result .= '<p class="content">'.$content.'</p>';
	$result .= '<div class="social-links">';
	$result .= '<a class="twitter" href="http://twitter.com/'.$twitter.'"><span class="icon-twitter"></span></a>';
	$result .= '<a class="linkedin" href="http://twitter.com/'.$linkedin.'"><span class="icon-linkedin"></span></a>';
	$result .= '</div>';
	$result .= '</div>';

	return $result;
}
add_shortcode( 'hh-person-item', 'hh_person_item_shortcode' );

function hh_material_item_shortcode( $atts ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"title"	=> '',
		"image" => '',
		"subtitle" => '',
		"description" => '',
		"content" => '',
		"documentation-link" => ''
	), $atts ) );

	$result = '<div class="hh-material-item">';
	$result .= '<img class="img-responsive img-material" src="/app/uploads/'.$image.'">';
	$result .= '<p class="function">'.$title.'</p>';
	$result .= '<h3 class="title">'.$subtitle.'</h3>';
	$result .= '<span class="hr"></span>';
	$result .= '<p class="description">'.$description.'</p>';
	$result .= '<p class="content">'.$content.'</p>';
	$result .= '<a href="'.$documentation-link.'"></a>';
	$result .= '</div>';

	return $result;
}
add_shortcode( 'hh-material-item', 'hh_material_item_shortcode' );

function hh_sponsor_block_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"level" => '',
		"price" => '',
		"background_color" => '',
		"link" => 'mailto:s.letelie@hackinghealth.ca',
		"button" => '',
	), $atts ) );

	$result = '<div class="hh-sponsor-block">
	          <div class="'.$background_color.'">
	            <div>
	              <p>'.$price.'</p>
	            </div>
	            <span>'.$level.'</span>
	          </div>';
	$result .= '<div>'. do_shortcode( $content ) .'</div>';
	if ($button != null) {
		$result .= '<div>
		            <a href="'.$link.'" class="btn btn-secondary btn-next">'.$button.'</a>
		          </div>';
	}
	$result .= '</div>';
	return $result;
}
add_shortcode( 'hh-sponsor-block', 'hh_sponsor_block_shortcode' );

/**
 * Wrap content into banner
 */
function gmm_stat_shortcode( $atts, $content = null ) {

	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"icon"   => '',
		"number" => '',
		"title"  => '',
		"text"   => '',
		"url"    => '',
	), $atts ) );


	// Wrap content in the styled banner
	$content = '<div class="number-stats">
					<p class="icon icon-' . $icon . '"></p>
					<p class="number"><span class="icon-arrow-left"></span>' . $number . '<span class="icon-arrow-right"></span></p>
					<p class="title">' . $title . '</p>
					<p class="text">' . $text . '</p>
					<a href="' . $url . '" class="btn btn-primary btn-block btn-next">' . __('Join the movement !', 'hacking') . '</a>
				</div>';

	return $content;
}
add_shortcode( 'stats', 'gmm_stat_shortcode' );


function hh_text_image_row_shortcode( $atts, $content = null ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"image" => '',
		"icon" => '',
		"color" => '',
		"orientation" => 'left'
	), $atts ) );

	$result = '<div class="row hh-text-image-row">';
	$push = ($orientation=='right')?'col-md-push-6':'';
	$result .= '<div class="col-md-6 '.$push.'">';
	$result .= do_shortcode( $content );
	$result .= '</div>';
	$pull = ($orientation=='right')?'col-md-pull-6':'';
	$result .= '<div class="col-md-6 '.$pull.'">';
	if ($image != null) {
		$result .= '<img src="'.$image.'" class="img-responsive">';
	} else if ($icon != null) {
		$result .= '<span class="icon-'.$icon.' '.$color.'"></span>';
	}
	$result .= '</div></div>';
	return $result;
}
add_shortcode( 'hh-text-image-row', 'hh_text_image_row_shortcode' );

function hh_text_image_banner_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"image" => '',
		"icon" => '',
		"cssbgcolor" => '',
		"color" => '',
		"orientation" => 'left',
		"title" => ''
	), $atts ) );

	$result = '<div class="hh-text-image-banner '.$cssbgcolor.'">';
	$result .= '<div class="row">';
	$pull = ($orientation=='right')?'col-md-push-8':'';
	$result .= '<div class="col-md-4 '.$pull.'">';
	if ($image != null) {
		$result .= '<img src="'.$image.'" class="img-responsive img-thumbnail">';
	} else if ($icon != null) {
		$result .= '<span class="icon-'.$icon.' '.$color.'"></span>';
	}
	$result .= '</div>';
	$push = ($orientation=='right')?'col-md-pull-4':'';
	$result .= '<div class="col-md-8 '.$push.'">';
	$result .= '<span class="hh-text-image-banner-title '.$color.'"">'.$title.'</span>';
	$result .= '<div class="hh-text-image-banner-text">'.do_shortcode( $content ).'</div>';
	$result .= '</div>';
	$result .= '</div></div>';
	return $result;
}
add_shortcode( 'hh-text-image-banner', 'hh_text_image_banner_shortcode' );


/**
 * Wrap content into banner
 */
function gmm_programme_wrapper_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	if( get_the_id() != getenv('WP_DEFI_SANTE')) {
		$title = '<h2 class="h1">' . __('Agenda', 'hacking') . '</h2>';
		$banner = 'banner';
	} else {
		$title = '';
		$banner = '';
	}

	$result = '<div class="' . $banner . ' relative">
		<div class="bottom-arrow"></div>
		<div class="container entry-content text-center">
			' . $title . '
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-default">'. do_shortcode( $content ) .'</div>
			</div>
		</div>
	</div>';

	return $result;
}
add_shortcode( 'programme-wrapper', 'gmm_programme_wrapper_shortcode' );

/**
 * Wrap content into banner
 */
function gmm_programme_lines_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"tab_id"      => '',
		"collapse_id" => '',
		"text"        => '',
		"date_number" => '',
		"date_day"    => '',
		"date_content"=> ''
	), $atts ) );

	if( get_the_id() != getenv('WP_DEFI_SANTE')) {
		$date_content = '<div class="date_number">' . $date_number . '<sup>' . __(ordinal_indicator($date_number), 'hacking') . '</sup></div>
					   <div class="date_day">' . $date_day . '</div>';
	} else {
		$date_content = '<div class="date_content">' . $date_content . '</div>';
	}

	// Wrap content in the styled banner
	$result = '<div class="panel-heading" role="tab" id="' . $tab_id . '">
					<div class="panel-title">
						<div class="date collapsed" data-toggle="collapse" data-parent="#accordion" href="#' . $collapse_id . '" aria-expanded="true" aria-controls="' . $collapse_id . '">
							' . $date_content . '
						</div>
						<span role="button" class="button" data-toggle="collapse" data-parent="#accordion" href="#' . $collapse_id . '" aria-expanded="true" aria-controls="' . $collapse_id . '">' . $text . '</span>
						<a class="icon-chevron text-right collapsed" data-toggle="collapse" data-parent="#accordion" href="#' . $collapse_id . '" aria-expanded="true" aria-controls="' . $collapse_id . '"></a>
					</div>
				</div>
				<div id="' . $collapse_id . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="' . $tab_id . '">
					<div class="panel-body">' . do_shortcode( $content ) . '</div>
				</div>';
	return $result;
}
add_shortcode( 'programme-lines', 'gmm_programme_lines_shortcode' );

/**
 * Wrap content into banner
 */
function gmm_programme_description_shortcode( $atts, $content = null ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"time"        => '',
		"description" => '',
		"photo"       => '',
	), $atts ) );

	// Wrap content in the styled banner
	$result = '<div class="program-lines">
					<div class="row">
						<div class="col-md-2">
							<div class="time">' . $time . '</div>
						</div>
						<div class="col-md-10 font-small">';
	if ($description != null) {
		$result .= '<p class="h2 text-left">' . $description . '</p>';
	}
	$result .= do_shortcode( $content ) . '
						</div>
					</div>
				</div>';
	return $result;
}
add_shortcode( 'programme-description', 'gmm_programme_description_shortcode' );

/**
 * Wrap content into banner
 */
function gmm_programme_description_sante_shortcode( $atts, $content = null ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"description" => '',
	), $atts ) );

	// Wrap content in the styled banner
	$result = '<div class="program-lines">';
	$result .= 		do_shortcode( $content ) . '
			   </div>';
	return $result;
}
add_shortcode( 'programme-description-sante', 'gmm_programme_description_sante_shortcode' );

/**
 * Wrap content into banner
 */
function gmm_banner_defi_sante_shortcode( $atts, $content = null ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"text1" => '',
		"text2" => '',
		"text3" => '',
	), $atts ) );

	// Wrap content in the styled banner
	$result = '<div class="banner-defi-sante">
					<div class="container">
						<div class="row">
							<div class="col-md-4 bottom-spacing-sm">
								<div class="number-circle">1</div>
								<p class="text-circle">' . $text1 . '</p>
							</div>
							<div class="col-md-4 bottom-spacing-sm">
								<div class="number-circle">2</div>
								<p class="text-circle">' . $text2 . '</p>
							</div>
							<div class="col-md-4 bottom-spacing-sm">
								<div class="number-circle">3</div>
								<p class="text-circle">' . $text3 . '</p>
							</div>
						</div>
					</div>
				</div>';

	return $result;
}
add_shortcode( 'banner-defi-sante', 'gmm_banner_defi_sante_shortcode' );

function hh_facebook_block_shortcode($atts) {
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"page"	=> 'hackinghealth',
		"height" => '400',
		"width" => '360',
		"lang" => 'en_US'
	), $atts ) );

	$result = '<div class="hh-social-block hh-facebook"><span class="icon-facebook"></span>';
	$result .= '<div id="fb-root"></div>';
	$result .= '<script>(function(d, s, id) {';
  $result .= 'var js, fjs = d.getElementsByTagName(s)[0];';
  $result .= 'if (d.getElementById(id)) return;';
	$result .= 'js = d.createElement(s); js.id = id;';
	$result .= 'js.src = "http://connect.facebook.net/'.$lang.'/sdk.js#xfbml=1&version=v2.5";';
	$result .= 'fjs.parentNode.insertBefore(js, fjs);';
	$result .= '}(document, "script", "facebook-jssdk"));</script>';
	$result .= '<div class="fb-page" data-href="https://www.facebook.com/'.$page.'" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="'.$height.'" data-width="'.$width.'"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/'.$page.'"><a href="https://www.facebook.com/'.$page.'">'.$page.'</a></blockquote></div></div>';
	$result .= '<a class="btn btn-next btn-social pull-right" href="https://www.facebook.com/'.$page.'" target="_blank">VISIT FACEBOOK</a>';
	$result .= '</div>';
	return $result;
}
add_shortcode( 'hh-facebook-block', 'hh_facebook_block_shortcode' );

function hh_twitter_block_shortcode($atts) {
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"user"	=> 'hackinghealthca',
		"height" => '400',
		"width" => '360',
		"lang" => 'en_US'
	), $atts ) );

	$result = '<div class="hh-social-block hh-twitter"><span class="icon-twitter"></span>';
	$result .= '<a class="twitter-timeline" href="https://twitter.com/'.$user.'" data-widget-id="638760039466037249" height="'.$height.'" width="'.$width.'" lang="'.$lang.'" data-screen-name="'.$user.'">Tweets '.$user.'</a>';
	$result .= '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
	$result .= '<a class="btn btn-next btn-social pull-right" href="https://www.twitter.com/'.$user.'" target="_blank">VISIT TWITTER</a>';
	$result .= '</div>';
	return $result;
}
add_shortcode( 'hh-twitter-block', 'hh_twitter_block_shortcode' );

function hh_youtube_block_shortcode($atts) {
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"user"	=> 'hackinghealth'
	), $atts ) );
	$result = '<script src="https://apis.google.com/js/platform.js"></script>';
	$result .= '<div class="g-ytsubscribe" data-channelid="'.$user.'" data-layout="full" data-count="default"></div>';
	return $result;
}
add_shortcode( 'hh-youtube-block', 'hh_youtube_block_shortcode' );


/**
 * Wrap content into banner
 */
function gmm_hardware_wrapper_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Wrap content in the styled banner
	$result = '<div class="banner-hardware">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div id="hardware-slider" class="entry-content">
							<h2 class="h1 text-center">' . __('Hardware & software', 'hacking') . '</h2>
							<ul class="slider">
								' . do_shortcode( $content ) . '</div>
							</ul>
						</div>
					</div>
				</div>';
	$result .= do_action( 'bxslider', 'hardware-slider');

	return $result;
}
add_shortcode( 'hardware-wrapper', 'gmm_hardware_wrapper_shortcode' );


/**
 * Wrap content into banner
 */
function gmm_hardware_list_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Wrap content in the styled banner
	$content = '<li class="hardware-item-slider"><div class="row">' . do_shortcode( $content ) . '</li></li>';

	return $content;
}
add_shortcode( 'hardware-list', 'gmm_hardware_list_shortcode' );


/**
 * Wrap content into banner
 */
function gmm_hardware_item_shortcode( $atts ) {

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"photo"   => '2015/08/img.png',
		"title"   => '',
		"content" => '',
		"url"     => '',
	), $atts ) );

	// Wrap content in the styled banner
	$content = '<div class="col-sm-6">
					<div class="item-hardware">
						<div class="item-block"><img class="img-reponsive img-hardware" src="/app/uploads/'.$photo.'" alt="img"></div>
						<h3 class="title">' . $title . '</h3>
						<span class="hr"></span>
						<p class="content">' . $content . '</p>
						<a target="_blank" href="' . $url . '" class="btn btn-primary btn-block btn-next">Read documentation</a>
					</div>
				</div>';

	return $content;
}
add_shortcode( 'hardware-item', 'gmm_hardware_item_shortcode' );

function hh_navigation_banner_shortcode($atts) {
	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"previous_path"	=> '',
		"next_path" => '',
		"chapter_path" => '',
		"previous_label"	=> '',
		"next_label" => '',
		"chapter_label" => ''
	), $atts ) );

	$result = '<div class="row hh-navigation-banner">';
	$result .= '<div class="col-md-4">';
	if ($previous_path != '') {
			$result .= '<a class="btn btn-primary btn-previous hh-navigation-previous" href="'.$previous_path.'">'.$previous_label.'</a>';
	}
	$result .= '</div>';
	$result .= '<div class="col-md-4 text-center">';
	if ($chapter_path != '') {
			$result .='<a class="btn btn-primary hh-navigation-chapter" href="'.$chapter_path.'">'.$chapter_label.'</a>';
	}
	$result .= '</div>';
	$result .= '<div class="col-md-4">';
	if ($chapter_path != '') {
			$result .= '<a class="btn btn-primary btn-next hh-navigation-next" href="'.$next_path.'">'.$next_label.'</a>';
	}
	$result .= '</div></div>';
	return $result;
}
add_shortcode( 'navigation-banner', 'hh_navigation_banner_shortcode' );

/**
 * Wrap content into simple div
 */
function gmm_honorable_price_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Div definition
	$class = "";

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"class" => $class,
	), $atts ) );


	// Wrap content in the styled banner
	$content = '<div class="'. $class .' relative text-center">
					<p class="title text-center">Mentions Honorables</p>
					'. do_shortcode( $content ) .'
				</div>';

	return $content;
}
add_shortcode( 'honorable-price', 'gmm_honorable_price_shortcode' );

/**
 * Wrap content into simple div
 */
function gmm_normal_prize_hackathon_shortcode( $atts, $content = null ) {
	// Don't return content if there isn't any
	if ( is_null($content) ) {
		return '';
	}

	// Div definition
	$class = "sponsor";

	// Extract shortcode attributes
	extract( shortcode_atts( array(
		"class" => $class,
		"image" => '',
		"link" => '',
		"title" => '',
	), $atts ) );


	// Wrap content in the styled banner
	$result = '<div class="'. $class .' normal-prize relative text-center">';
	if ($link != '') {
		$result .= '<a href="'. $link .'" target="_blank">';
	}
	$result .= '<img class="img-reponsive aligncenter prize-img" src="/app/uploads/'.$image.'" alt="'. $title .'">';
	if ($link != '') {
		$result .= '</a>';
	}
	$result .= '<p class="title">' . $title . '</p>';
  $result .= '<div class="more-description">';
  $result .= '<div class="description">' . do_shortcode( $content ) . '</div>';
	$result .= '</div>';
	$result .= '</div>';

	return $result;
}
add_shortcode( 'normal-prize', 'gmm_normal_prize_hackathon_shortcode' );


/**
 * Return ordinal indicator of integer
 */
function ordinal_indicator( $integer ) {
	// Return empty character if integer is null
    if ( is_null($integer) ) {
		return '';
	}
    # Return empty character if integr is not an integer
    if( ! filter_var($integer, FILTER_VALIDATE_INT) ){
        return '';
    }

    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($integer % 100) >= 11) && (($integer%100) <= 13))
        return $integer. 'th';
    else
        return $ends[$integer % 10];
}
