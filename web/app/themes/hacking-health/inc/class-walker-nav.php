<?php
/**
 * A class for displaying various tree-like structures.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */
class GMM_Walker_Nav_Menu extends Walker_Nav_Menu {
	
	/**
	 * Bootstrap dropdown vars
	 */
	const DIVIDER_CLASSNAME = 'divider';
	const DROPDOWN_CLASSNAME = 'dropdown';
	const NAV_HEADER_CLASSNAME = 'nav-header';
	
	private $start_el_class = false;
	public $has_children = false;

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		$id_field = $this->db_fields['id'];
		$id = $element->$id_field;
		$this->has_children = (bool) ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]);
		parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		
		switch ( $this->start_el_class ) {
			/*case self::DROPDOWN_CLASSNAME :
				$class_name = 'dropdown-menu';
				break;
			*/
			default:
				$class_name = 'dropdown-menu';
				break;
		}

		$output .= "\n$indent<ul class=\"$class_name\">\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;

		$this->start_el_class = false;

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		if ( $this->has_children ) {	
			$dropdown_classname = self::DROPDOWN_CLASSNAME;
			if ( $depth > 0 ) {
				$dropdown_classname .= '-submenu';
			}
			$classes[] = $dropdown_classname;
		}
		$classes[] = 'menu-item-'. $item->ID;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li' . $value . $class_names .'>';
		
		$attributes = '';
		$is_nav_header = in_array( self::NAV_HEADER_CLASSNAME, $classes );
		$is_divider = in_array( self::DIVIDER_CLASSNAME, $classes );

		if ( !$is_nav_header ) {
			$attributes .= ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		}

		$prepend = '';
		$append = '';
		$item_tag = 'a';

		if ( $is_nav_header ) {
			$item_tag = 'span';
		} else if ( $is_divider ) {
			
		//} else if ( in_array( self::DROPDOWN_CLASSNAME, $classes ) ) {
		} else if ( $this->has_children ) {
			$this->start_el_class = self::DROPDOWN_CLASSNAME;
			$attributes .= ' class="dropdown-toggle" data-toggle="dropdown"';
			$append .= ' <b class="caret"></b>';
		}
		$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

		if($depth != 0) {
			$description = $append = $prepend = "";
		}

		$item_output = $args->before;
		if ( !$is_divider ) {
			$item_output .= '<' . $item_tag . $attributes .'>';
			$item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
			$item_output .= $description.$args->link_after;
			$item_output .= '</' . $item_tag .'>';
		}
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
