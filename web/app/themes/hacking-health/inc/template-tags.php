<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

require_once('class-walker-nav.php');

if ( ! function_exists( 'gmm_content_nav' ) ):
/**
 * Display navigation to next/previous pages when applicable
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_content_nav( $nav_id ) {
	global $wp_query;

	$nav_class = 'site-navigation paging-navigation';
	if ( is_single() )
		$nav_class = 'site-navigation post-navigation';

	?>
	<nav role="navigation" id="<?php echo $nav_id; ?>" class="<?php echo $nav_class; ?>">
	<?php if ( is_home() && $nav_id != 'nav-below' ) : ?>
		<h1 class="assistive-text"><?php _e( 'Actualités', 'hacking' ); ?></h1>
	<?php endif; ?>

	<?php if ( is_single() ) : // navigation links for single posts ?>

		<?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Article précédent', 'hacking' ) . '</span> %title' ); ?>
		<?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Article suivant', 'hacking' ) . '</span>' ); ?>

	<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

		<?php if ( get_next_posts_link() ) : ?>
		<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Anciens articles', 'hacking' ) ); ?></div>
		<?php endif; ?>

		<?php if ( get_previous_posts_link() ) : ?>
		<div class="nav-next"><?php previous_posts_link( __( 'Articles récents <span class="meta-nav">&rarr;</span>', 'hacking' ) ); ?></div>
		<?php endif; ?>

	<?php endif; ?>

	</nav><!-- #<?php echo $nav_id; ?> -->
	<?php
}
endif; // gmm_content_nav

if ( ! function_exists( 'gmm_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Rétrolien:', 'hacking' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Modifier)', 'hacking' ), ' ' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer>
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 40 ); ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'hacking' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'hacking' ); ?></em>
					<br />
				<?php endif; ?>

				<div class="comment-meta commentmetadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time datetime="<?php comment_time( 'c' ); ?>">
					<?php
						/* translators: 1: date, 2: time */
						printf( __( '%1$s at %2$s', 'hacking' ), get_comment_date(), get_comment_time() ); ?>
					</time></a>
					<?php edit_comment_link( __( '(Edit)', 'hacking' ), ' ' );
					?>
				</div><!-- .comment-meta .commentmetadata -->
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for gmm_comment()


$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
global $gmm_comment_form;
$gmm_comment_form = array(
	'fields'		=>  array(
		'author'		=> '<div class="comment-form-author control-group">' . '<label for="author" class="control-label">' . __( 'Name', 'hacking' ) . '</label> ' .
		            '<div class="controls"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' . ( $req ? '<span class="required help-inline">*</span>' : '' ) . '</div></div>',
		
		'email'			=> '<div class="comment-form-email control-group"><label for="email" class="control-label">' . __( 'Email', 'hacking' ) . '</label> ' . 
		            '<div class="controls"><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />' . ( $req ? '<span class="required help-inline">*</span>' : '' ) .'</div></div>',
		
		'url'			=> '<div class="comment-form-url control-group"><label for="url" class="control-label">' . __( 'Website', 'hacking' ) . '</label>' .
		            '<div class="controls"><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div></div>'
	),
	'comment_field'	=> '<div class="comment-form-comment control-group"><label for="comment" class="control-label">' . _x( 'Comment', 'hacking' ) . '</label><div class="controls"><textarea id="comment" class="span12" name="comment" cols="45" rows="8" aria-required="true"></textarea></div></div>'
);

if ( ! function_exists( 'gmm_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_posted_on() {
	printf( __( '<time class="entry-date" datetime="%3$s">%4$s</time>', 'hacking' ),
		esc_url( get_permalink() ),
		esc_attr( get_the_title() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'Voir tous les articles de %s', 'hacking' ), get_the_author() ) ),
		esc_html( get_the_author() )
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so gmm_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so gmm_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in gmm_categorized_blog
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'gmm_category_transient_flusher' );
add_action( 'save_post', 'gmm_category_transient_flusher' );

/**
 * Filtering a Class in Navigation Menu Item
 * 
 * @since GMM's Bootstrap 1.0
 */
function gmm_special_nav_class($classes, $item){
	$current_key = array_search('current-menu-item', $classes);
	if ( $current_key !== false ) {
		$classes[$current_key] = 'active';
	}
	return $classes;
}
add_filter('nav_menu_css_class' , 'gmm_special_nav_class' , 10 , 2);


/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
function gmm_excerpt_length( $length ) {
	return 11;
}
add_filter( 'excerpt_length', 'gmm_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 */
function gmm_continue_reading_link() {
	$btn_classes = 'btn';
	if ( is_sticky() && !is_search() ) {
		$btn_classes .= ' btn-primary btn-large';
	}
	return '<a class="'.$btn_classes.'" href="'. esc_url( get_permalink() ) . '">' . __( 'Lire la suite', 'hacking' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and gmm_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
function gmm_excerpt_more( $more ) {
	return ' &hellip;';
}
add_filter( 'excerpt_more', 'gmm_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
function gmm_get_the_excerpt( $output ) {
	$output = apply_filters('the_content', $output);
	if ( ! is_attachment() && ! is_page() ) {
		$output .= '<p class="more-link">' . gmm_continue_reading_link() . '</p>';
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'gmm_get_the_excerpt' );

/**
 * Extract and truncate the content or the excerpt
 */
function gmm_get_extract( $length, $more = '...' ) {
	if (!has_excerpt()) {
		$excerpt = strip_shortcodes( get_the_content() );
	} else {
		$excerpt = get_the_excerpt();
	}

	$excerpt = str_replace("&nbsp;", "", $excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = trim(mb_substr($excerpt,0,$length));
	$excerpt = rtrim($excerpt, ',');
	$excerpt = rtrim($excerpt, '.').$more;

	return $excerpt;
}

function gmm_extract( $length, $more = '...' ) {
	echo gmm_get_extract($length, $more);
}

/**
 * Customize the content
 */
function gmm_the_content( $content ) {
	return $content;
}
add_filter( 'the_content', 'gmm_the_content' );


/**
 * Get post relationship
 */
function gmm_post_belongs($post, $post_belongs) {
	$post_belongs_id = 0;

	foreach ($post_belongs as $key) {
		$post_belongs_id = (int) get_post_meta($post->ID, '_wpcf_belongs_'.$key.'_id', true);
		if ( $post_belongs_id > 0 ) {
			break;
		}
	}

	return $post_belongs_id;
}

/**
 * Add Bxslider scripts
 */
function gmm_bxslider($slider_id) {
	if ( !$slider_id ) {
		$slider_id = 'slider';
	}
?>
<script type="text/javascript">
	var sliderId = sliderId || [];
	sliderId.push('#<?php echo $slider_id; ?>');
</script>

<?php
	wp_enqueue_script( 'default-slider' );
}
add_action( 'bxslider', 'gmm_bxslider', 10, 1 );

/**
 * Retrieve the listing post's link
 */
function gmm_get_post_archive_link() {
	
	if ( get_option('show_on_front') == 'page' ) {
		$posts_page_id = get_option( 'page_for_posts');
		$posts_page_url = home_url( get_page_uri($posts_page_id) );
	} else {
		$posts_page_url = '';
	}

	return $posts_page_url;
}

/**
 * Delete HTML tags in wp_title
 */
function gmm_wp_title($title, $sep) {
	$title = html_entity_decode($title);
	$title = strip_tags($title);
	return $title;
}
add_filter( 'wp_title', 'gmm_wp_title', 1000, 2 );

/**
 * Customize Contact form 7 response output
 */
function gmm_form_response_output($output, $class, $content) {
	return '<div class="form-group row"><div class="col-md-8 col-lg-9 col-md-offset-4 col-lg-offset-3">'. $output .'</div></div>';
}
add_filter( 'wpcf7_form_response_output', 'gmm_form_response_output', 10, 4 );

/**
 * Customize CSS class Contact form 7
 */
// add_filter( 'wpcf7_form_class_attr', 'gmm_wpcf7_form_class_attr' );
// function gmm_wpcf7_form_class_attr( $class ) {
// 	$class .= ' form-horizontal';
// 	return $class;
// }

/**
 * Customize loader Contact form 7
 */
// add_filter( 'wpcf7_ajax_loader', 'gmm_wpcf7_ajax_loader' );
// function gmm_wpcf7_ajax_loader() {
// 	global $gmm_theme_url;
// 	return $gmm_theme_url . 'img/ajax-loader.gif';
// }


/**
 * Highlight content
 */
// function gmm_highlight_shortcode( $atts, $content = null ) {
// 	if ( is_null($content) ) {
// 		return '';
// 	}

// 	$content = trim($content);

// 	$highlight_class = 'highlight clear';

// 	$content = '<section class="'. $highlight_class .'">'. $content .'</section>';
// 	return $content;
// }
// add_shortcode( 'highlight', 'gmm_highlight_shortcode' );