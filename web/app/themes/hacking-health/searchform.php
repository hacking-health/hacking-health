<?php
/**
 * The template for displaying search forms in GMM's Bootstrap
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */
?>
<form method="get" id="searchform" class="navbar-search form-search" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<div class="input-append">
		<input type="text" class="search-query" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Mots-clés&hellip;', 'hacking' ); ?>" />
		<input type="submit" class="btn" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Rechercher', 'hacking' ); ?>" />
	</div>
</form>