<?php
/**
 * The Template for displaying all single posts.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */


$link = get_post_meta( get_the_id(), 'wpcf-eventbrite-link', true );
$place = get_post_meta( get_the_id(), 'wpcf-event-place', true );
$hackathon_page = get_post_meta( get_the_id(), 'wpcf-challenge-page-url', true );
$hackathon_formulaire = get_post_meta( get_the_id(), 'wpcf-challenge-submission-url', true );
$event_date = get_post_meta( get_the_id(), 'wpcf-event-date', true );
$event_end_date = get_post_meta( get_the_id(), 'wpcf-event-end-date', true );
$event_hour = get_post_meta( get_the_id(), 'wpcf-event-hour', true );
$programme = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-program', true ));
$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$hardware = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-hardware-banner', true ));
$social_sharing = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-social-sharing', true ));
$newsletter_link = get_post_meta( get_the_id(), 'wpcf-newsletter-link', true );
$navigation_link = get_post_meta( get_the_id(), 'wpcf-navigation-links', true );
$contact_form = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-contact-form', true ));

$diff_time = time() - (int)$event_date;

if ( trim($event_date) ) {
	$date_format = gmm_current_language() == 'fr' ? 'l j F Y' : 'l F j Y';
	$event_date = date_i18n($date_format, $event_date);
}

if ( trim($event_end_date) ) {
	$date_format = gmm_current_language() == 'fr' ? 'l j F Y' : 'l F j Y';
	$event_end_date = date_i18n($date_format, $event_end_date);
}

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<?php if ($navigation_link != '') { ?>
					<div class="navigation-links">
						<div class="container">
							<?php echo $navigation_link; ?>
						</div>
					</div>
				<?php } ?>
				<div class="single-event-banner" style="background-image: url('<?php echo $url; ?>')">
					<div class="container">
						<div class="text-center">
							<h1 class="title"><?php echo get_the_title(); ?></h1>
							<p class="date">
								<?php echo $event_date;?>
								<?php if ($event_end_date != null) {
									echo ' - ' . $event_end_date;
								} ?>
								<?php if ($event_hour != null) {
									echo ' - ' . $event_hour;
								} ?>
							</p>
							<p class="place"><?php echo $place;?></p>
							<?php if ($diff_time < 0 && $link != '') { ?>
							<div class="row">
								<div class="col-md-offset-4 col-md-4">
									<a target="_blank" href="<?php echo $link; ?>" class="btn btn-primary btn-next btn-block top-spacing"><?php _e('Register Now!', 'hacking'); ?></a>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<!--<div class="banner-news">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">
							<div class="row">
								<div class="col-md-offset-2 col-md-4">
									<a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>" class="btn btn-secondary btn-block btn-prev"><?php _e('Previous event', 'hacking'); ?></a>
								</div>
								<div class="col-md-4">
									<a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>" class="btn btn-secondary btn-block btn-next"><?php _e('Next event', 'hacking'); ?></a>
								</div>
							</div>
						</div>
					</div>-->
				</div>
				<?php echo the_content(); ?>
				<?php if ($hackathon_page != '' || $hackathon_formulaire != '') { ?>
				<div class="banner-story">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">
							<div class="row">
								<?php if ($hackathon_page != '' && $hackathon_formulaire != '') { ?>
								<div class="col-md-offset-2 col-md-4">
									<a target="_blank" href="<?php echo $hackathon_page; ?>" class="btn btn-primary btn-block btn-next"><?php _e('Check out upcoming projects', 'hacking'); ?></a>
								</div>
								<div class="col-md-4 half-top-margin-sm">
									<a target="_blank" href="<?php echo $hackathon_formulaire; ?>" class="btn btn-primary btn-block btn-next"><?php _e('Submit your project', 'hacking'); ?></a>
								</div>
								<?php } elseif ($hackathon_page == '' && $hackathon_formulaire != '') { ?>
								<div class="col-md-offset-4 col-md-4">
									<a target="_blank" href="<?php echo $hackathon_formulaire; ?>" class="btn btn-primary btn-block btn-next"><?php _e('Submit your project', 'hacking'); ?></a>
								</div>
								<?php } else { ?>
								<div class="col-md-offset-4 col-md-4">
									<a target="_blank" href="<?php echo $hackathon_page; ?>" class="btn btn-primary btn-block btn-next"><?php _e('Check out upcoming projects', 'hacking'); ?></a>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if($programme != '') { ?>
					<?php echo $programme; ?>
				<?php } ?>
				<?php if ( $newsletter_link != null ) { ?>
				<div class="banner-newsletter">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">
							<h2 class="h1 text-center"><?php _e('Keep informed', 'hacking'); ?></h2>
							<form class="mailchimp" action="<?php echo $newsletter_link; ?>" method="post" target="_blank">
								<div class="form-group row">
									<div class="col-md-offset-2 col-md-8">
										<input class="form-control" name="EMAIL" type="email" placeholder="<?php _e('Insert your email address', 'hacking'); ?>">
										<input type="hidden" name="FROM" value="EVENT">
										<button type="submit" name="submitNewsletter" class="btn btn-primary btn-newsletter">
											<span class="icon-arrow-right-circle"></span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php get_template_part( 'parts/banner-leader' );?>
				<?php if ( $social_sharing != null ) { ?>
					<?php echo $social_sharing; ?>
				<?php } ?>
				<?php wp_reset_query(); ?>
				<?php get_template_part( 'content', 'city-partners-event' );?>
				
				<?php
				// Formulaire de contact
				?> 
				<?php if ($contact_form != '') { ?>
				<div class="banner-contact">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">
							<h2 class="h1 text-center"><?php _e('Contact us', 'hacking'); ?></h2>
							<?php echo $contact_form; ?>
						</div>
					</div>
				</div>
				<?php } else { ?>
					<?php get_template_part( 'parts/banner-contact' ); ?>
				<?php } ?> 

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php get_footer(); ?>
