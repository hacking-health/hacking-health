<?php
/**
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<section id="post-<?php the_ID(); ?>" <?php post_class('col-md-4'); ?>>
	<div class="city bottom-spacing" style="background-image: url('<?php echo $url; ?>')">
		<p class="title"><?php echo get_the_title();?></p>
		<a href="<?php echo get_the_permalink(); ?>" class="btn btn-tertiary btn-block btn-next"><?php _e('More', 'hacking');?></a>
	</div>
</section><!-- #post-<?php the_ID(); ?> -->
