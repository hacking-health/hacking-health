<?php
/**
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$link = get_post_meta( get_the_id(), 'wpcf-eventbrite-link', true );
$place = get_post_meta( get_the_id(), 'wpcf-event-place', true );
$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

$event_date = get_post_meta( get_the_id(), 'wpcf-event-date', true );

$event_number = date_i18n('j', $event_date);
$event_day = date_i18n('l', $event_date);
$event_month = date_i18n('F Y', $event_date);

?>

<section id="post-<?php the_ID(); ?>" style="background-image: url('<?php echo $url; ?>')" <?php post_class('event bottom-spacing'); ?>>
	<div class="col-md-4">
		<div class="row date">
			<div class="col-md-6">
				<span class="number"><?php echo $event_number; ?></span>
			</div>
			<div class="col-md-6">
				<span class="day"><?php echo $event_day; ?></span>
				<span class="month"><?php echo $event_month;?></span>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<p class="title"><?php echo get_the_title();?></p>
		<p class="place"><?php echo $place; ?></p>
	</div>
	<div class="col-md-3 top-spacing">
		<a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary btn-block btn-next top-spacing"><?php _e('More', 'hacking');?></a>
	</div>
</section><!-- #post-<?php the_ID(); ?> -->
