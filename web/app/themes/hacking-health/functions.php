<?php
/**
 * GMM's Bootstrap functions and definitions
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since GMM's Bootstrap 1.0
 */
if ( ! isset( $content_width ) )
	$content_width = 940; /* pixels */

if ( ! function_exists( 'gmm_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_setup() {
	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom shortcodes for this theme.
	 */
	require( get_template_directory() . '/inc/common-shortcodes.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/tweaks.php' );

	/**
	 * Custom Theme Options
	 */
	// require( get_template_directory() . '/inc/theme-options/theme-options.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on GMM's Bootstrap, use a find and replace
	 * to change 'hacking' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'hacking', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	//add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Remove WP generator
	 */
	remove_action( 'wp_head', 'wp_generator' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Menu principal', 'hacking' ),
	) );

	/**
	 * Custom content admin CSS
	 */
	add_editor_style( 'css/wp-editor.css' );

	/**
	 * Add support for the Aside Post Formats
	 */
	//add_theme_support( 'post-formats', array( 'aside', ) );

	//add_post_type_support( 'page', 'excerpt' );
}
endif; // gmm_setup
add_action( 'after_setup_theme', 'gmm_setup' );

/**
 * Add X-UA-Compatible head for IE
 * @since GMM's Bootstrap 1.0
 */
function gmm_header_xua() {
	if ( isset($_SERVER['HTTP_USER_AGENT']) &&
		 strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false ) {
		header('X-UA-Compatible: IE=edge,chrome=1');
	}
}
add_action( 'send_headers', 'gmm_header_xua' );

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_widgets_init() {

	// unregister all default WP Widgets
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Tag_Cloud');
	unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Search');

	// unregister_widget('WP_Widget_Text');

    // register site widgets
	register_sidebar( array(
		'name' => __( 'Barre latérale', 'hacking' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="row %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<p class="muted">',
		'after_title' => '</p>',
	) );
}
add_action( 'widgets_init', 'gmm_widgets_init' );

/**
 * Implement the Custom Header feature
 *
 * @since GMM's Bootstrap 1.0
 */
//require( get_template_directory() . '/inc/custom-header.php' );

/**
 * Add meta robots on front-page and paged page to prevent duplicate content
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_robots() {
	global $page, $paged;
	if ( $page > 0 && $paged > 0 && !is_front_page() && !is_category() ) {
		echo '<meta name="robots" content="noindex,nofollow" />'."\n";
	}
}
add_action('wp_head', 'gmm_robots');

/**
 * Enqueue CSS files with wp_enqueue_style function
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_styles() {
	global $wp_styles;

	$theme = wp_get_theme();
	$theme_ver = $theme->Version;
	$theme_css = get_template_directory_uri().'/css/';

	wp_deregister_style( 'open-sans' );
	wp_enqueue_style( 'open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:800,700,400,300', false, $theme_ver);
	wp_enqueue_style( 'roboto-slab', 'https://fonts.googleapis.com/css?family=Roboto+Slab:700,400,300,100', false, $theme_ver);
	wp_enqueue_style( 'rubik', 'https://fonts.googleapis.com/css?family=Rubik:300,400,400i,700', false, $theme_ver);
	wp_enqueue_style( 'roboto-condensed', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i', false, $theme_ver);

	wp_enqueue_style( 'theme-global', $theme_css . 'theme-global.css', false, $theme_ver );

	wp_enqueue_style( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css', false, $theme_ver);

	wp_enqueue_style( 'slider', $theme_css . "jquery.bxslider.css", false, $theme_ver );

	if ( is_front_page() || get_post_type() == 'city' ) {
		wp_enqueue_style( 'maximage', $theme_css . "jquery.maximage.css", false, $theme_ver );
	}

	// IE style sheets
	// wp_register_style( 'lte-ie9', $theme_css . 'lte-ie9.css', false, $theme_ver, 'screen' );
	// $GLOBALS['wp_styles']->add_data( 'lte-ie9', 'conditional', 'lte IE 9' );
	// wp_enqueue_style('lte-ie9');

	// wp_register_style( 'lte-ie8', $theme_css . 'lte-ie8.css', false, $theme_ver, 'screen' );
	// $GLOBALS['wp_styles']->add_data( 'lte-ie8', 'conditional', 'lte IE 8' );
	// wp_enqueue_style('lte-ie8');

	// Hide WP version and put theme version
	$wp_styles_queue = $wp_styles->queue;
	if ( in_array( 'admin-bar', $wp_styles_queue ) ) {
		$wp_styles->registered['admin-bar']->ver = $theme_ver;
	}
}
add_action('wp_enqueue_scripts', 'gmm_styles');

/**
 * Enqueue JS files with wp_enqueue_script function
 * Load latest jQuery version
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_scripts() {
	global $wp_scripts;

	$theme = wp_get_theme();
	$theme_ver = $theme->Version;
	$theme_js = get_template_directory_uri().'/js/';
	$development_js = get_template_directory_uri().'/dev-js/';
	$post_type = get_post_type();
	$development_mode = (getenv( 'WP_ENV' ) == 'development');

	wp_deregister_script( 'jquery' );

	$is_lte_ie9 = preg_match('/(?i)msie [2-9]/',$_SERVER['HTTP_USER_AGENT']);
	if ( $is_lte_ie9 ) {
		$jquery_version = '1.11.1';
	} else {
		$jquery_version = '2.1.4';
	}

	if ($development_mode) {
		wp_register_script( 'jquery', $development_js."jquery.js", NULL, NULL );
	} else {
		wp_register_script( 'jquery', "http://code.jquery.com/jquery-$jquery_version.min.js", NULL, NULL );
	}
	wp_register_script( 'jquery-easing', "//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js", array('jquery'), NULL );
	wp_register_script( 'jquery-migrate', "http://code.jquery.com/jquery-migrate-1.2.1.min.js", array('jquery'), NULL );

	wp_register_script( 'modernizr', "//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js", NULL, NULL, true);
	wp_register_script( 'bxslider', $theme_js . "vendor/bxslider/jquery.bxslider.js", array('jquery'), '4.1.2', true );

	wp_register_script( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', NULL, NULL);

	wp_register_script( 'slider', $theme_js . "slider.js", array('modernizr', 'bxslider'), $theme_ver, true);
	wp_register_script( 'default-slider', $theme_js . "default-slider.js", array('modernizr', 'bxslider'), $theme_ver, true);
	wp_register_script( 'gmm-plugins', $theme_js . "plugins.js", array('jquery', 'modernizr'), $theme_ver, true);
	wp_register_script( 'prize', $theme_js . "prize.js", array('jquery'), $theme_ver, true);

	wp_enqueue_script( 'jquery' );

	if ( is_front_page() || get_post_type() == 'city' ) {
		wp_enqueue_script( 'maximage', $theme_js . "vendor/maximage/jquery.maximage.js", array('jquery'), '2.0', true );
		wp_enqueue_script( 'video-maximage', $theme_js . "video.js", array('modernizr', 'bxslider'), $theme_ver, true);
	}

	if ( get_post_type() == 'event' ) {
		wp_enqueue_script('prize');
	}

	// Enqueue only if JS bugs
	if ( $is_lte_ie9 ) {
		wp_enqueue_script( 'jquery-migrate' );
	}

	wp_enqueue_script( 'modernizr' );
	wp_enqueue_script( 'bootstrap-transition', $theme_js . "vendor/bootstrap/transition.js", NULL, $theme_ver, true);
	wp_enqueue_script( 'bootstrap-collapse', $theme_js . "vendor/bootstrap/collapse.js", NULL, $theme_ver, true);
	wp_enqueue_script( 'select2' );
	wp_enqueue_script( 'gmm-plugins' );

	// Hide WP version and put theme version
	$wp_scripts_queue = $wp_scripts->queue;
	if ( in_array( 'admin-bar', $wp_scripts_queue ) ) {
		$wp_scripts->registered['admin-bar']->ver = $theme_ver;
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts', 'gmm_scripts');

/**
 * Customize the query before execution
 */
function gmm_pre_get_posts( $query ) {

	if ( $query->is_main_query() ) {

		if ( $query->is_tax( 'category-event-city' ) ) {
			$query->set('post_type', 'event');
			$query->set('posts_per_page', -1);
			$query->set('order', 'DESC');
			$query->set('orderby', 'meta_value_num');
			$query->set('meta_key', 'wpcf-event-date');

			if ( isset($_GET['past-events']) ) {
				$query->set('past_events', true);
				$query->set('meta_query', array(
					array(
						'key'       => 'wpcf-event-date',
						'value'     => time(),
						'compare'   => '<',
						'type'      => 'NUMERIC',
					),
				));
			}
		}

		// Display all events
		if ( isset($query->query['post_type']) && ('city' == $query->query['post_type']) ) {
			$query->set('posts_per_page', -1);
		}
	}
}
add_action('pre_get_posts', 'gmm_pre_get_posts');

/**
 * Add Favicon and Apple touch icons
 *
 * @see http://realfavicongenerator.net/
 * @since GMM's Bootstrap 1.0
 */
function gmm_head() {
	$pingback_url = get_bloginfo( 'pingback_url' );
	$favicons_path = get_template_directory_uri() . '/favicons/';
	$cache_ver = '?v=2.0';

	echo <<<LINKS
<link rel="pingback" href="{$pingback_url}" />
<link rel="apple-touch-icon" sizes="57x57" href="{$favicons_path}apple-touch-icon-57x57.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="60x60" href="{$favicons_path}apple-touch-icon-60x60.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="72x72" href="{$favicons_path}apple-touch-icon-72x72.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="76x76" href="{$favicons_path}apple-touch-icon-76x76.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="114x114" href="{$favicons_path}apple-touch-icon-114x114.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="120x120" href="{$favicons_path}apple-touch-icon-120x120.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="144x144" href="{$favicons_path}apple-touch-icon-144x144.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="152x152" href="{$favicons_path}apple-touch-icon-152x152.png{$cache_ver}">
<link rel="apple-touch-icon" sizes="180x180" href="{$favicons_path}apple-touch-icon-180x180.png{$cache_ver}">
<link rel="icon" type="image/png" href="{$favicons_path}favicon-32x32.png{$cache_ver}" sizes="32x32">
<link rel="icon" type="image/png" href="{$favicons_path}favicon-194x194.png{$cache_ver}" sizes="194x194">
<link rel="icon" type="image/png" href="{$favicons_path}favicon-96x96.png{$cache_ver}" sizes="96x96">
<link rel="icon" type="image/png" href="{$favicons_path}android-chrome-192x192.png{$cache_ver}" sizes="192x192">
<link rel="icon" type="image/png" href="{$favicons_path}favicon-16x16.png{$cache_ver}" sizes="16x16">
<link rel="manifest" href="{$favicons_path}manifest.json{$cache_ver}">
<link rel="shortcut icon" href="{$favicons_path}favicon.ico{$cache_ver}">
<meta name="apple-mobile-web-app-title" content="Hacking Health">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="application-name" content="Hacking Health">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{$favicons_path}mstile-144x144.png{$cache_ver}">
<meta name="msapplication-config" content="{$favicons_path}browserconfig.xml{$cache_ver}">
<meta name="theme-color" content="#ffffff">

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

LINKS;

}
add_action('wp_head', 'gmm_head');

/**
 * Custom login page
 *
 * @since GMM's Bootstrap 1.0
 */
function gmm_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/css/wp-login.css" />';
}
// add_action('login_head', 'gmm_login');

/**
 * Remove unused admin meta box, menu-page
 */
function gmm_remove_unused_box() {
	remove_meta_box( 'tagsdiv-post_tag', 'post', 'normal' );
	remove_meta_box( 'pll-tagsdiv-post_tag', 'post', 'normal' );
	remove_meta_box( 'commentsdiv', 'post', 'normal' );
	remove_meta_box( 'commentstatusdiv', 'post', 'normal' );

	remove_menu_page('edit-comments.php');

	remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );
}
add_action( 'admin_menu', 'gmm_remove_unused_box' );

/**
 * Get current language
 */
function gmm_current_language() {
	if ( function_exists('pll_current_language') ) {
		return pll_current_language();
	} else {
		return get_bloginfo('language');
	}
}

/**
 * Create a shortcode to disable WordPress automatic formatting on posts.
 */
function gmm_raw($content) {
	$new_content = '';
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

	foreach ($pieces as $piece) {
		if (preg_match($pattern_contents, $piece, $matches)) {
			$new_content .= $matches[1];
		} else {
			$new_content .= $piece;
		}
	}

	return $new_content;
}

//remove_filter('the_content', 'wpautop');
//remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'gmm_raw', 99);
