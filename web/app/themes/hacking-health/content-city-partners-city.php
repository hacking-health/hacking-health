<?php
/**
 * The template for displaying the partners in a city page.
 *
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$partner_url = get_post_meta( get_the_id(), 'wpcf-partner-url', true );

$city_tax_partner_id = (int)get_post_meta( get_the_id(), 'wpcf-id-category-partner-city', true );

$show_partner_level = (int)get_post_meta( get_the_id(), 'wpcf-show-partner-level', true );

$partner_args = array(
	'post_type'      => 'partner',
	'posts_per_page' => 50,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-partner-city',
			'field'    => 'id',
			'terms'    => $city_tax_partner_id,
		)
	)
);

$partner_query = new WP_Query( $partner_args );

?>
<?php if ( $partner_query->have_posts() ) { ?>

	<div class="banner-partners">
		<div class="container">
			<div class="entry-content">
				<h2 class="h1 text-center"><?php _e('Our partners', 'hacking'); ?></h2>

	<?php if ( $show_partner_level == 1 ) { ?>
	<div class="row">

		<h2 class="text-center"><?php _e('Platinum partners', 'hacking'); ?></h2>

		<?php while ( $partner_query->have_posts() ) : $partner_query->the_post(); ?>
		<?php $partner_level = get_post_meta( get_the_id(), 'wpcf-partner-level', true ); ?>

			<?php if ( $partner_level == 'platinum' ) { ?>

				<?php get_template_part( 'content', 'city-partners' );?>

			<?php } ?>

		<?php endwhile; // end of the loop. ?>

	</div>
	<div class="row">

		<h2 class="text-center"><?php _e('Gold partners', 'hacking'); ?></h2>

		<?php while ( $partner_query->have_posts() ) : $partner_query->the_post(); ?>
		<?php $partner_level = get_post_meta( get_the_id(), 'wpcf-partner-level', true ); ?>

			<?php if ( $partner_level == 'gold' ) { ?>

				<?php get_template_part( 'content', 'city-partners' );?>

			<?php } ?>

		<?php endwhile; // end of the loop. ?>

	</div>
	<div class="row">

		<h2 class="text-center"><?php _e('Silver partners', 'hacking'); ?></h2>

		<?php while ( $partner_query->have_posts() ) : $partner_query->the_post(); ?>
		<?php $partner_level = get_post_meta( get_the_id(), 'wpcf-partner-level', true ); ?>

			<?php if ( $partner_level == 'silver' ) { ?>

				<?php get_template_part( 'content', 'city-partners' );?>

			<?php } ?>

		<?php endwhile; // end of the loop. ?>

	</div>
	<div class="row">

		<h2 class="text-center"><?php _e('Bronze partners', 'hacking'); ?></h2>

		<?php while ( $partner_query->have_posts() ) : $partner_query->the_post(); ?>
		<?php $partner_level = get_post_meta( get_the_id(), 'wpcf-partner-level', true ); ?>

			<?php if ( $partner_level == 'bronze' ) { ?>

				<?php get_template_part( 'content', 'city-partners' );?>

			<?php } ?>

		<?php endwhile; // end of the loop. ?>

	</div>
	<div class="row">

		<h2 class="text-center"><?php _e('Partners', 'hacking'); ?></h2>

		<?php while ( $partner_query->have_posts() ) : $partner_query->the_post(); ?>
		<?php $partner_level = get_post_meta( get_the_id(), 'wpcf-partner-level', true ); ?>

			<?php if ( $partner_level == 'event' ) { ?>

				<?php get_template_part( 'content', 'city-partners' );?>

			<?php } ?>

		<?php endwhile; // end of the loop. ?>

	</div>
	<?php } else {?>
		<div class="row">

			<?php while ( $partner_query->have_posts() ) : $partner_query->the_post(); ?>
			<?php $partner_level = get_post_meta( get_the_id(), 'wpcf-partner-level', true ); ?>

					<?php get_template_part( 'content', 'city-partners' );?>

			<?php endwhile; // end of the loop. ?>

		</div>
	<?php } ?>

							</div>
						</div>
					</div>

<?php } ?>
