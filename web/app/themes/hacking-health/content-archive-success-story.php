<?php
/**
 * The template for displaying the success story listing.
 *
 * @package Hacking Health
 * @since Hacking Health 1.0
 */

$content = strip_shortcodes(get_the_excerpt());

?>


<div class="entry-content">
	<div class="col-md-6">
		<div class="item-news">
			<?php echo get_the_post_thumbnail( get_the_id(), 'full', array('class' => 'img-responsive center-block') ); ?>
			<div class="info-news clearfix">
				<p class="date"><?php _e('Success story', 'hacking'); ?></p>
				<p class="title"><?php echo mb_substr(strip_tags(get_the_title()), 0, 52).'...'; ?></p>
				<p class="content"><?php echo mb_substr(strip_tags($content), 0, 210).'...'; ?></p>
				<a href="<?php echo get_the_permalink(); ?>" class="text-right more-info"><span class="icon-plus"></span></a>
			</div>
		</div>
	</div>
</div>
