<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

get_header(); ?>
	<div class="banner-story">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<div class="entry-content">

					<div class="img-banner archive-success-story-banner">
						<div class="container">
							<div class="text-center">
								<p class="title">Hacking Health Factory</p>
								<p class="baseline">Applies evidence based innovation in healthcare</p>
							</div>
						</div>
					</div>
					<header class="entry-header">
						<h1 class="entry-title"><?php _e('Success stories', 'hacking'); ?></h1>
					</header><!-- .entry-header -->
					
					<div class="container">
						<div class="row">
							<?php while ( have_posts() ) : the_post(); ?>

								<?php get_template_part( 'content', 'archive-success-story' ); ?>

							<?php endwhile; // end of the loop. ?>
						</div>
					</div>
				</div>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->
	</div>

<?php get_footer(); ?>