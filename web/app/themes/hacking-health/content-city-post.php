<?php
/**
 * The template for displaying the posts in a city page.
 *
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

?>

<div class="col-md-4">
	<div class="item-news">
		<?php echo get_the_post_thumbnail( get_the_id(), 'img-news', array('class' => 'img-responsive img-temoignage center-block') ); ?>
		<div class="info-news clearfix">
			<p class="date"><?php echo strip_tags(gmm_posted_on()); ?></p>
			<p class="title"><?php echo mb_substr(strip_tags(get_the_title()), 0, 100).'...'; ?></p>
			<a href="<?php echo get_the_permalink(); ?>" class="text-right more-info"><span class="icon-plus"></span></a>
		</div>
	</div>
</div>