// Add classes on the hackathon prize blocks for the animation's goal
$(function(){

    if ( $(window).width() < 767 ) {
        return;
    }

    // Toggle visible content
    var $desc = $('.more-description'),
        $normal_prize = $('.normal-prize'),
        min_height = 125,
        visible_height = 180,
        btn_exists = false,
        btn_active = null;

    $desc.each(function(index, element){
        var $btn = $('<span class="btn btn-tertiary btn-toggle btn-down"></span>'),
            $element = $(element),
            $rte = $element.find('.description');

        if ($rte.height() < min_height) {
            return;
        }

        btn_exists = true;

        $btn.data('desc', $element);
        $btn.data('viewport', $element.closest('.normal-prize'));
        $btn.data('rte', $rte);
        $element.append($btn);
    });

    if (btn_exists) {
        toggle_desc();
    }

    function toggle_desc() {
        $('body').on('click', function(event){
            var $target = $(event.target);

            if ( !$target.hasClass('btn-toggle') ) {
                toggleOut(btn_active);
            }
        });

        $('.btn-toggle').on('click', function(event){
            var cat_height,
                $this = $(this);

            if ( $this.is(btn_active) ) {
                toggleOut($this);
                cat_height = visible_height;
            } else {
                toggleOut(btn_active);

                $this.removeClass('btn-down').addClass('btn-up');
                $normal_prize.addClass('shadow');
                $this.data('viewport').removeClass('shadow').addClass('in-front-of');
                $this.data('desc')
                    .height($this.data('rte').height()+70)
                    .addClass('active');
                btn_active = $this;
            }
        })
    }

    function toggleOut($btn) {
        if ( $btn == null ) {
            return;
        }

        $btn.removeClass('btn-up').addClass('btn-down');
        $normal_prize.removeClass('shadow');
        $btn.removeClass('active');
        $btn.data('viewport').removeClass('in-front-of');
        $btn.data('desc')
            .height((visible_height)-50)
            .removeClass('active');
        btn_active = null;
    }

});