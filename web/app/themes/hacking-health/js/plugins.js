// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

$(function(){

    var $select_city = $('#select-city');

    if ( $select_city.length && typeof select_cites_options != "undefined" ) {
        $select_city.select2({
            data: select_cites_options
        }).on("select2:select", function (e) {
            var url = e.params.data.url;
            location.href = url;
        });
    }
});
