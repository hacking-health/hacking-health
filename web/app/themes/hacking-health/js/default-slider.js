(function($){
	if ( sliderId == undefined ) {
		return;
	}

	var $window = $(window);

	$(sliderId).each(function( index, id ) {
		var $slider = $(id),
			$bxSlider = $slider.find('.slider'),
			$bxSliderView,
			$bxSliderWrapper,
			$currentSlide,
			$controls,
			hasMinSlideCount = false,
			isElastic = false,
			hasHoverListener = false,
			startSlider = function(){
				$bxSlider.startAuto(false);
			},
			stopSlider = function(){
				$bxSlider.stopAuto(false);
			},
			bxSliderVars = {
				mode: 'fade',
				pager: true,
				auto: true,
				speed: 500,
				pause: 3000,
				controls: false,

				onSliderLoad: function(currentIndex){
					var items = $bxSlider.find('> li');
					$bxSliderWrapper = $slider.find('.bx-wrapper');
					$bxSliderView = $slider.find('.bx-viewport');
					$currentSlide = items.eq(currentIndex);
					hasMinSlideCount = items.length > 1;

					if ( !hasMinSlideCount ) {
						$bxSliderWrapper.find('.bx-controls').remove();
					}
				},
				onSlideBefore: function($slideElement, oldIndex, newIndex){
					$currentSlide = $slideElement;
				}
			};

			if ( id == '#event-slider') {
				bxSliderVars.controls = true;
				bxSliderVars.prevText = '';
				bxSliderVars.nextText = '';
			}

			$bxSlider.bxSlider(bxSliderVars);
	});

})(jQuery);