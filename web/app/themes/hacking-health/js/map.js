function venueMap(lgUrl, smUrl) {
  var map,
  mapCenter = new google.maps.LatLng(48.5778309,7.7410011),
  mapOptions = {
    center: mapCenter,
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false,
  },
  mapInfowindow = new google.maps.InfoWindow({content: ""}),
  locations = [
    {
      lat: 48.576504,
      lng: 7.739521,
      title: '<strong>Formations & Hackathon</strong>',
      address: 'Faculté de médecine',
      address2: "4, rue kirschleger <br/>67000 Strasbourg <br/> FRANCE",
    },
    {
      lat: 48.5757855,
      lng: 7.7407523,
      title: '<strong>Conferences</strong>',
      address: "IRCAD",
      address2: "1 Place de l'hôpital<br/>67000 Strasbourg <br/> FRANCE"
    }
  ],
  markers = [],
  markerIcon = {
    LG: lgUrl,
    SM: smUrl
  },
  deviceType = {
    LG: 'lg',
    MD: 'md',
    SM: 'sm',
    XS: 'xs'
  },
  currentDeviceType;

  function initialize() {
    map = new google.maps.Map(document.getElementById("map-canvas"),
        mapOptions);

    var location,agence;

    for (i = 0; i < locations.length; i++) {
      location = locations[i];
      location.icon = markerIcon.LG;
      location.content = '<div>'
        +'<div class="bold">'+ location.title +'</div>'
        +'<div>'+ location.address +'<br>'
        + location.address2 +'<br>'
        +'</div></div>';
      agence = new Agence(location, map, mapInfowindow);

      markers.push(agence.getMarker());
    }

    if ( Modernizr.mq('only all') ) {
      google.maps.event.addDomListener(window, 'resize', resize);
      google.maps.event.addListenerOnce(map, 'idle', resize);
    }
  }
  function Agence ( infos, map, infoWindow ) {
    this.infos = infos;
    this.map = map;
    this.infoWindow = infoWindow;

    this.setMarker();

    if ( this.infoWindow ) {
      this.setHandler();
    }
  }
  Agence.prototype.setMarker = function() {
    var markerOptions = {
      position: new google.maps.LatLng(this.infos.lat, this.infos.lng),
      icon: this.infos.icon,
      map: this.map
    }
    this.marker = new google.maps.Marker(markerOptions);
  }
  Agence.prototype.setHandler = function() {
    var self = this;
    google.maps.event.addListener(self.marker, 'click', function() {
      self.infoWindow.setContent(self.infos.content);
      self.infoWindow.open(self.map, self.marker);
    });
  }
  Agence.prototype.getMarker = function() {
    return this.marker;
  }
  function updateMarkers(iconPath) {
    for (i = 0; i < markers.length; i++) {
      var marker = markers[i];
      marker.setIcon(iconPath);
    }
  }
  function getDeviceType() {
    if ( Modernizr.mq('screen and (max-width: 767px)') ) {
      return deviceType.XS;
    } else if ( Modernizr.mq('screen and (max-width: 991px)') ) {
      return deviceType.SM;
    } else if ( Modernizr.mq('screen and (min-width: 1200px)') ) {
      return deviceType.LG;
    } else {
      return deviceType.MD;
    }
  }
  function resize() {
    var prevDeviceType = currentDeviceType;
    currentDeviceType = getDeviceType();

    google.maps.event.trigger(map, 'resize');
    map.setCenter(mapCenter);

    if ( prevDeviceType != currentDeviceType
       || currentDeviceType == deviceType.XS
       || currentDeviceType == deviceType.MD ) {
      var center = mapCenter,
        zoom = mapOptions.zoom,
        icon = markerIcon.LG;

      map.setCenter(center);
      map.setZoom(zoom);
      updateMarkers(icon);
    }
  }
  google.maps.event.addDomListener(window, 'load', initialize);
}
