<?php
/**
 * The template for displaying events pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$name = $wp_query->queried_object->name;

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<div class="entry-content">

					<div class="img-banner archive-event-banner">
						<div class="container">
							<div class="text-center">
								<p class="title">
								<?php if ( get_query_var('past_events') ) : ?>
									<?php printf( __( 'Past events from %s', 'hacking' ), $name); ?>
								<?php else : ?>
									<?php printf( __( 'All events from %s', 'hacking' ), $name); ?>
								<?php endif; ?>
								</p>
							</div>
						</div>
					</div>
					<header class="entry-header">
						<h1 class="entry-title">
						<?php if ( have_posts() ) : ?>
							<?php _e('All events', 'hacking'); ?>
						<?php else : ?>
							<?php _e('There are no events.', 'hacking'); ?>
						<?php endif; ?>
						</h1>
					</header><!-- .entry-header -->
					
					<div class="container">
						<?php if ( get_query_var('past_events') ) : ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php get_template_part( 'content', 'archive-past-event' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php else : ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php get_template_part( 'content', 'archive-event' ); ?>

							<?php endwhile; // end of the loop. ?>

						<?php endif; ?>
					</div>
				</div>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php get_footer(); ?>