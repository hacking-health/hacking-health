<?php

$theme_url = get_template_directory_uri().'/';
$arguments = array(
	'post_type'			=> 'slide',
	'posts_per_page'	=> -1,
	'paged'				=> 1,
	'post_status'		=> 'publish',
	'order'				=> 'ASC',
	'orderby'			=> 'menu_order',
);

$slides = query_posts($arguments);

if ( have_posts() ) {
	$items = '';
	$nav_items = '';
	$slides_count = count($slides);
	$count = 0;
	
	while ( have_posts() ) : the_post();

	if ( !has_post_thumbnail() ) {
		$slides_count--;
		continue;
	}

	$post_belongs_id = gmm_post_belongs($post, array('post', 'page'));
	$post_url = $post_belongs_id > 0 ? get_permalink( $post_belongs_id ) : '';
	$post_link = '';
	$post_link_close = '';
	$post_link_target = '';
	$post_caption = '';
	if ( !trim($post_url) ) {
		$post_url = get_post_meta($post->ID, 'wpcf-post_url', true);
	}
	
	if ( $post_url ) {
		$post_url_text = get_post_meta($post->ID, 'post_url_text', true);
		$post_url_text = !trim($post_url_text) ? __('En savoir +', 'hacking') : $post_url_text;
		$post_link_target = get_post_meta($post->ID, 'wpcf-post_url_blank', true) ? ' target="_blank"' : '';

		$post_link = '<a class="btn btn-block" href="'. $post_url .'"'. $post_link_target .'>'. $post_url_text .'</a>';
	}
	
	$post_content = get_the_content();
	if ( trim($post_content) ) {
		$post_content = apply_filters( 'the_content', get_the_content() );
	}

	if ( trim($post_content) || trim($post_url) ) {
		$post_caption = $post_content;
	}

	$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
	$post_thumbnail = wp_get_attachment_image_src($post_thumbnail_id, 'slide');
	$post_thumbnail = '<img src="' . $post_thumbnail[0] . '" class="attachment-slide wp-post-image" alt="" />';

	$item_class = array( 'item' );
	if ( $count == 0 ) {
		$item_class[] = 'active';
	}
	$item_class = implode(' ', $item_class);
	$items .= <<<ITEM
		<li class="{$item_class}">
			{$post_thumbnail}
			<div class="bx-caption">
				<div class="container bx-content">
					<div class="slide-description">{$post_caption}</div>
					<div class="slide-link">{$post_link}{$post_link_close}</div>
				</div>
			</div>
		</li>
ITEM;
	
	$count++;
	
	endwhile;

	if ( $slides_count > 0 ) {
?>
<aside id="home-slider" class="slider">
	<ul class="bxslider-single">
		<?php echo $items ?>
	</ul>
</aside>
<?php 
		do_action( 'bxslider', 'home-slider' );

	}
}

wp_reset_query();
