<div class="video-container">
	<!-- HTML5 Video Code "Video For Everybody" http://camendesign.com/code/video_for_everybody -->
	<video autoplay="autoplay" loop="loop" autobuffer="autobuffer" muted="muted" width="640" height="360">
	    <source src="/app/uploads/hackinghealth.mp4" type="video/mp4" />
	    <source src="/app/uploads/hackinghealth.webm" type="video/webm" />
	    <source src="/app/uploads/hackinghealth.ogv" type="video/ogg" />
	    <object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="640" height="360">
	        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
	        <param name="allowFullScreen" value="true" />
	        <param name="wmode" value="transparent" />
	        <param name="flashVars" value="config={'playlist':['%2Fapp%2Fuploads%2Fhackinghealth.jpg',{'url':'%2Fapp%2Fuploads%2Fhackinghealth.mp4','autoPlay':false}]}" />
	        <img alt="Hacking Health" src="/app/uploads/hackinghealth.jpg" width="640" height="360" title="No video playback capabilities, please download the video below" />
	    </object>
	</video>
	<div class="container fix-position">
		<div class="text-center">
			<p class="text-video">
				<span class="font-big-video"><span class="icon-hh"></span><?php _e('Hacking Health', 'hacking'); ?></span>
				<br>
				<span class="font-video"><?php _e('Bringing innovation to healthcare', 'hacking'); ?></span>
			</p>
			<a href="https://www.youtube.com/watch?v=01kSXfG_LOs" target="_blank" class="btn btn-tertiary btn-next double-top-spacing"><?php _e('Watch our video !', 'hacking'); ?></a>
		</div>
	</div>
</div>
