<?php
/**
 * The template for displaying the people's listing.
 *
 * @package Hacking Health
 * @since Hacking Health 1.0
 */

$args = array (
	'post_type'              => 'people',
	'post_status'            => 'publish',
	'order'                  => 'DESC',
	'meta_key'               => 'wpcf-display-homepage'
);
// The Queries
$query = new WP_Query( $args );
$results = $query->found_posts;
?>

<div class="banner-temoignage">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div id="temoignage-slider" class="entry-content row">
			<div class="col-md-offset-2 col-md-8">
				<h2 class="h1 text-center"><?php _e('Hacking Health Team', 'hacking'); ?></h2>
				<ul class="slider">
				<?php if ( $query->have_posts() ) { ?>

					<?php $i = 0; ?>

					<?php while ( $query->have_posts() ) : $query->the_post(); ?>

						<?php if(($i % 6) == 0 || $i == 0) { ?>
							<li class="temoignage-item-slider">
								<div class="row">
						<?php } ?>

						<?php $function = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-fonction-leader', true )); ?>
						<?php $description = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-description-leader', true ));
							  $description = trim(mb_substr(strip_tags($description),0,45)); ?>
						<?php $twitter = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-twitter-link', true )); ?>
						<?php $linkedin = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-linkedin-link', true )); ?>
						<?php $behance = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-behance-link', true )); ?>
						<?php $content = strip_tags(get_the_content()); ?>

								<div class="col-sm-6">
									<div class="item-temoignage">
										<?php echo get_the_post_thumbnail ( get_the_id(), 'thumbnail', array('class' => 'img-responsive img-temoignage') ); ?>
										<p class="function"><?php echo strip_tags($function); ?></p>
										<h3 class="title"><?php echo strip_tags(get_the_title()); ?></h3>
										<span class="hr"></span>
										<p class="description"><?php echo strip_tags($description); ?></p>
										<p class="content"><?php if(strlen(strip_tags(get_the_content())) > 154) { echo trim(mb_substr($content, 0, 154)).'...'; } else { echo $content; } ?></p>
										<div class="social-links">
											<a class="twitter" href="<?php echo strip_tags($twitter); ?>"><span class="icon-twitter"></span></a>
											<a class="linkedin" href="<?php echo strip_tags($linkedin); ?>"><span class="icon-linkedin"></span></a>
										</div>
									</div>
								</div>

						<?php $i ++; ?>

						<?php if(($i % 6) == 0 || $results == $i ) { ?>
								</div>
							</li>
						<?php } ?>

					<?php endwhile; // end of the loop. ?>

				<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php
do_action( 'bxslider', 'temoignage-slider'); ?>
