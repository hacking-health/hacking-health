<div class="row">
  <div class="col-sm-4">
  </div>
  <div class="col-sm-4">
    <p>
      <a href="https://desjardins.com" target="_blank"><img class="aligncenter wp-image-4998" src="http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo.jpg" alt="Desjardins-Logo"
          srcset="http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo.jpg 3145w, http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo-300x62.jpg 300w, http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo-1024x212.jpg 1024w, http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo-360x74.jpg 360w, http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo-250x52.jpg 250w, http://hackinghealth.ca//app/uploads/2015/10/Desjardins-Logo-200x41.jpg 200w"
          sizes="(max-width: 3145px) 100vw, 3145px" /></a><br />
  </div>
  <div class="col-sm-4">
  </div>
</div>
<div class="row">
  <div class="col-sm-3">
  </div>
  <div class="col-sm-3">
    <p>
      <a href="https://www.bdc.ca/en/pages/home.aspx" target="_blank"><img class="aligncenter wp-image-8147 size-full" src="http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo.png"
          alt="201701bdclogo" width="800" height="500" srcset="http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo.png 800w, http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo-300x188.png 300w, http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo-768x480.png 768w, http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo-531x332.png 531w, http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo-240x150.png 240w, http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo-250x156.png 250w, http://hackinghealth.ca//app/uploads/2016/10/201701BDClogo-200x125.png 200w"
          sizes="(max-width: 800px) 100vw, 800px" /></a>
  </div>
  <div class="col-sm-3">
    <a href="http://hhaccelerator.com" target="_blank"><img class="aligncenter wp-image-8096 size-full" src="http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent.png"
        alt="hha-logo-transparent" width="1280" height="800" srcset="http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent.png 1280w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-300x188.png 300w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-768x480.png 768w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-1024x640.png 1024w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-531x332.png 531w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-240x150.png 240w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-250x156.png 250w, http://hackinghealth.ca//app/uploads/2016/10/HHA-logo-transparent-200x125.png 200w"
        sizes="(max-width: 1280px) 100vw, 1280px" /></a><br />
  </div>
  <div class="col-sm-4">
  </div>
</div>
<div class="row">
  <div class="col-sm-3">
  </div>
  <div class="col-sm-2">
    <a href="http://www.fasken.com/"><img class="aligncenter wp-image-8145 size-full" src="http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo.png"
        alt="201701faskenlogo" width="800" height="500" srcset="http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo.png 800w, http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo-300x188.png 300w, http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo-768x480.png 768w, http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo-531x332.png 531w, http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo-240x150.png 240w, http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo-250x156.png 250w, http://hackinghealth.ca//app/uploads/2016/10/201701FaskenLogo-200x125.png 200w"
        sizes="(max-width: 800px) 100vw, 800px" /></a><br />
  </div>
  <div class="col-sm-1">
  </div>
  <div class="col-sm-2">
    <a href="http://www.merck.ca/english/pages/home.aspx"><img class="aligncenter size-full wp-image-8146" src="http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo.png"
        alt="201701mercklogo" width="800" height="500" srcset="http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo.png 800w, http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo-300x188.png 300w, http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo-768x480.png 768w, http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo-531x332.png 531w, http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo-240x150.png 240w, http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo-250x156.png 250w, http://hackinghealth.ca//app/uploads/2016/10/201701Mercklogo-200x125.png 200w"
        sizes="(max-width: 800px) 100vw, 800px" /></a><br />
  </div>
  <div class="col-sm-4">
  </div>
</div>
<div class="row">
  <div class="col-sm-5">
  </div>
  <div class="col-sm-1">
    <a href="http://www.marsdd.com" target="_blank"><br />
    </a>
    <a href="http://www.marsdd.com" target="_blank"><img class="wp-image-2555 aligncenter" src="http://hackinghealth.ca//app/uploads/2015/12/mars-logo-136x136.png" alt="mars-logo"
      /></a><br />
  </div>
  <div class="col-sm-5">
  </div>
</div>
