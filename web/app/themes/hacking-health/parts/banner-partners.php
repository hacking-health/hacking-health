<div class="banner-partners">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div class="entry-content">
			<h2 class="h1 text-center"><?php _e('Our national partners', 'hacking'); ?></h2>
				<hr>
					<?php if (get_bloginfo('language') == "en-GB") {?>
			<div class="row">
				<div class="col-md-12" style="text-align: center;">
<span style="font-size: xx-large;">Strength is in numbers. </span><br>
<span style="font-size: x-large;">Great things cannot happen alone, which is why Hacking Health benefits from the support of amazing partners and sponsors.</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-4 col-md-4">
					<br><br>
					<a class="btn btn-primary btn-block btn-next" href="http://hackinghealth.ca/fr/sponsors" target="_blank">ABOUT OUR SPONSORS</a>
				</div>
			</div>
					<?php } elseif (get_bloginfo('language') == "fr-FR") {?>
			<div class="row">
				<div class="col-md-12" style="text-align: center;">
<span style="font-size: xx-large;">L'union fait la force. </span><br>
<span style="font-size: x-large;">C'est pourquoi Hacking Health est privilégié d'être soutenu par nos sponsors et partenaires.</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-4 col-md-4">
					<a class="btn btn-primary btn-block btn-next" href="http://hackinghealth.ca/fr/sponsors" target="_blank">DÉCOUVREZ NOS SPONSORS</a>
				</div>
			</div>
					<?php }?>
					<hr>
		</div>
	</div>
</div>
