<div class="banner-newsletter">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div class="entry-content">
			<h2 class="h1 text-center"><?php _e('Keep informed on the events', 'hacking'); ?></h2>
			<form class="mailchimp" action="#" method="post" target="_blank">
				<div class="form-group row">
					<div class="col-md-offset-2 col-md-8">
						<input class="form-control" name="EMAIL" type="email" placeholder="<?php _e('Insert your email address', 'hacking'); ?>">
						<button type="submit" name="submitNewsletter" class="btn btn-primary btn-newsletter">
							<span class="icon-arrow-right-circle"></span>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
