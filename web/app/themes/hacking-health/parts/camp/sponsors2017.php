<?php
$allpartner_link = get_post_meta( get_the_id(), 'wpcf-allpartner-link', true );
$partner_link = get_post_meta( get_the_id(), 'wpcf-partner-link', true );
?>
  <div class="white-banner banner relative entry-content">
    <div class="bottom-arrow"></div>
    <div class="container">
      <h2 class="h1" style="text-align: center;">
        <?php _e('Our sponsors', 'hacking'); ?>
      </h2>
      <div class="row">
        <?php if ($allpartner_link != '') { ?>
        <div class="col-md-6 text-center">
          <a href="<?php echo $allpartner_link; ?>" class="btn btn-tertiary btn-next">
            <?php _e('See all partners', 'hacking'); ?>
          </a>
        </div>
        <?php } ?>
        <?php if ($partner_link != '') { ?>
        <div class="col-md-6 text-center">
          <a href="<?php echo $partner_link; ?>" class="btn btn-tertiary btn-next">
            <?php _e('Become a sponsor', 'hacking'); ?>
          </a>
        </div>
        <?php } ?>
      </div>
      <div class="row silver">
        <div class="silver-col col-sm-4 col-sm-offset-0">
          <a href="http://esante.gouv.fr" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="max-height: 200px;" src="http://esante.gouv.fr/sites/default/files/logo_ASIP.jpg" alt="europe" height="96" /></a>
        </div>
        <div class="silver-col col-sm-4 col-sm-offset-0">
          <a href="http://www.lilly.fr" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="max-height: 200px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Eli_Lilly_and_Company.svg/2000px-Eli_Lilly_and_Company.svg.png" alt="europe" height="96" /></a>
        </div>
        <div class="silver-col col-sm-4 col-sm-offset-0">
          <a href="http://europe-en-alsace.eu/" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="max-height: 200px;" src="http://europe-en-alsace.eu/wp-content/uploads/2015/11/logo-UE-JPEG.jpg" alt="europe" height="90" /></a>
        </div>
      </div>
      <br><br><br>
      <div class="row gold">
        <div class="gold-col col-sm-12 col-sm-offset-0">
          <a href="http://www.marand.com/" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://www.marand.com/assets/images/logo_marand.svg" alt="marand" style="height: 75px; max-height: 200px;" /></a>
        </div>
      </div>
      <div class="row bronze">
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.eurogroupconsulting.fr/" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="https://upload.wikimedia.org/wikipedia/commons/b/b4/Logo-Eurogroup-Consulting.png" alt="eurogroup" style="height: 70px;"/></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://enterprise.alcatel-lucent.com/countrysite/fr/" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="https://upload.wikimedia.org/wikipedia/commons/7/7d/Alcatel_Lucent_Enterprise_Logo.svg" alt="alcatel lucent entreprise" style="height: 70px;"/></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.sanofi.fr" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="http://hackinghealth.ca/app/uploads/2015/09/sanofi2.jpg" alt="sanofi" style="height: 70px;"/></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.pharmagest.com" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="http://www.pharmagest.com/custom/production/images/haut/logoPharmagest.png" alt="pharmagest" style="height: 70px;"/></a>
        </div>
      </div>
      <h2 class="h1" style="text-align: center;">
        <?php _e('Our partners', 'hacking'); ?>
      </h2>
      <div class="row simple">
        <div class="simple-col col-sm-3 col-sm-offset-0">
          <a href="http://www.ihu-strasbourg.eu/ihu/" target="_blank"><img class="size-bronze-partner wp-image-677 aligncenter" src="/app/uploads/2015/11/ihu-200x64.jpg" alt="ihu"/></a>
        </div>
        <div class="simple-col col-sm-3 col-sm-offset-0">
          <a href="http://www.ircad.fr/" target="_blank"><img class="size-medium wp-image-673 aligncenter" src="/app/uploads/2015/09/ircad1.jpg" alt="ircad" /></a>
        </div>
        <div class="simple-col col-sm-3 col-sm-offset-0">
          <a href="https://www.unistra.fr/" target="_blank"><img class="size-medium wp-image-1909 aligncenter" src="https://upload.wikimedia.org/wikipedia/fr/thumb/d/d1/Universit%C3%A9_de_Strasbourg_(logo).svg/langfr-280px-Universit%C3%A9_de_Strasbourg_(logo).svg.png" alt="unistra" /></a>
        </div>
        <div class="gold-col col-sm-3 col-sm-offset-0">
          <a href="http://strasbourg.eu" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://hackinghealth.ca/app/uploads/2016/01/Strasbourg.eu-Signature-Verte-250x62.jpg" alt="europe" /></a>
        </div>
      </div>
      <br><br><br>
      <div class="row simple">
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.lajavaness.com/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="http://hackinghealth.ca//app/uploads/2016/09/logo_LJN-5-200x125.png" alt="la javaness" class="prize-img" style="height: 80px;"/></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.alsacebusinessangels.com/" target="_blank"><img class="size-bronze-partner wp-image-1952 aligncenter" src="http://hackinghealth.ca//app/uploads/2016/09/ABA-logo.jpg" alt="alsace business angels" class="prize-img" style="height: 80px;" /></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.semia-incal.com/" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="/app/uploads/2016/01/logo_semia.png" alt="semia" class="prize-img" style="height: 50px;" /></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.lesml.org" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="http://hackinghealth.ca/app/uploads/2016/09/logo-sml.png" alt="sml" class="prize-img" style="height: 50px;" /></a>
        </div>
      </div>
    </div>
  </div>