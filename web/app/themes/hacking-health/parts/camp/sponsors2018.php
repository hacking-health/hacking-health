<?php
$allpartner_link = get_post_meta( get_the_id(), 'wpcf-allpartner-link', true );
$partner_link = get_post_meta( get_the_id(), 'wpcf-partner-link', true );
?>
  <div class="white-banner banner relative entry-content">
    <div class="bottom-arrow"></div>
    <div class="container">
      <h2 class="h1" style="text-align: center;">
        <?php _e('Our sponsors', 'hacking'); ?>
      </h2>
      <div class="row">
        <?php if ($allpartner_link != '') { ?>
        <div class="col-md-6 text-center">
          <a href="<?php echo $allpartner_link; ?>" class="btn btn-tertiary btn-next">
            <?php _e('See all partners', 'hacking'); ?>
          </a>
        </div>
        <?php } ?>
        <div class="col-md-12 text-center">
          <a href="mailto:seb@healthfactory.io" class="btn btn-tertiary btn-next">
            <?php _e('Become a sponsor', 'hacking'); ?>
          </a>
        </div>
      </div>
      <br><br><br>
      <div class="row silver">
        <div class="silver-col col-sm-2 col-sm-offset-0">
          <a href="http://esante.gouv.fr" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="height: 135px;" src="http://esante.gouv.fr/sites/default/files/logo_ASIP.jpg" alt="asip" /></a>
        </div>
        <div class="silver-col col-sm-4 col-sm-offset-0">
          <a href="http://www.mylan.fr" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="height: 100px;" src="http://hackinghealth.ca/app/uploads/2017/11/logo_small_reg_2x_072517.png" alt="mylan" /></a>
        </div>
        <div class="silver-col col-sm-4 col-sm-offset-0">
          <a href="http://www.al-entreprise.fr" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="height: 80px;" src="http://hackinghealth.ca/app/uploads/2017/11/Alcatel_logo.png" alt="ale" /></a>
        </div>
        <div class="silver-col col-sm-2 col-sm-offset-0">
          <a href="http://www.lilly.fr" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="height: 90px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Eli_Lilly_and_Company.svg/2000px-Eli_Lilly_and_Company.svg.png" alt="lilly" /></a>
        </div>
      </div>
      <div class="row silver">
        <div class="silver-col col-sm-12 col-sm-offset-0">
          <a href="https://www.groupe-vyv.fr/" target="_blank"><img class="size-medium wp-image-674 aligncenter" style="height: 165px;" src="http://hackinghealth.ca//app/uploads/2018/02/Groupe-VYV_RVB.jpg" alt="VYV" /></a>
        </div>
      </div>
      <div class="row gold">
        <div class="gold-col col-sm-6 col-sm-offset-0">
          <a href="http://becare.me/" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://hackinghealth.ca//app/uploads/2018/02/becare.png" alt="becare" style="height: 60px;" /></a>
        </div>
        <div class="gold-col col-sm-6 col-sm-offset-0">
          <a href="http://www.chru-strasbourg.fr/" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://www.chru-strasbourg.fr/themes/hus/logo.png" alt="hus" style="height: 120px;" /></a>
        </div>
      </div>
      <div class="row bronze">
      <div class="bronze-col col-sm-6 col-sm-offset-0">
          <a href="http://www.ihu-strasbourg.eu/ihu/" target="_blank"><img class="size-bronze-partner wp-image-677 aligncenter" src="http://hackinghealth.ca//app/uploads/2018/02/ihuStrasbourg_logo.png" alt="ihu" style="height: 100px;"/></a>
        </div>
        <div class="bronze-col col-sm-6 col-sm-offset-0">
          <a href="http://www.eurogroupconsulting.fr/" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="https://upload.wikimedia.org/wikipedia/commons/b/b4/Logo-Eurogroup-Consulting.png" alt="eurogroup" style="height: 100px;"/></a>
        </div>
      </div>
      <h2 class="h1" style="text-align: center;">
        <?php _e('Our partners', 'hacking'); ?>
      </h2>
      <div class="row simple">
        <div class="simple-col col-sm-4 col-sm-offset-0">
          <a href="http://www.ircad.fr/" target="_blank"><img class="size-medium wp-image-673 aligncenter" src="/app/uploads/2015/09/ircad1.jpg" alt="ircad" /></a>
        </div>
        <div class="simple-col col-sm-4 col-sm-offset-0">
          <a href="https://www.clinique-rhena.fr/fr" target="_blank"><img class="size-medium wp-image-673 aligncenter" src="http://www.diaconesses.fr/blog/wp-content/uploads/2014/06/logo-rhena-300x115.jpg" alt="ircad" /></a>
        </div>
        <div class="simple-col col-sm-4 col-sm-offset-0">
          <a href="https://www.unistra.fr/" target="_blank"><img class="size-medium wp-image-1909 aligncenter" src="https://upload.wikimedia.org/wikipedia/en/thumb/9/98/University_of_Strasbourg_logo.svg/1280px-University_of_Strasbourg_logo.svg.png" alt="unistra" /></a>
        </div>
      </div>
      <br><br><br>
      <div class="row simple">
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.lajavaness.com/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="http://hackinghealth.ca//app/uploads/2016/09/logo_LJN-5-200x125.png" alt="la javaness" class="prize-img" style="height: 80px;"/></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.alsacebusinessangels.com/" target="_blank"><img class="size-bronze-partner wp-image-1952 aligncenter" src="http://hackinghealth.ca//app/uploads/2016/09/ABA-logo.jpg" alt="alsace business angels" class="prize-img" style="height: 80px;" /></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.semia-incal.com/" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="/app/uploads/2016/01/logo_semia.png" alt="semia" class="prize-img" style="height: 50px;" /></a>
        </div>
        <div class="bronze-col col-sm-3 col-sm-offset-0">
          <a href="http://www.lesml.org" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="http://hackinghealth.ca/app/uploads/2016/09/logo-sml.png" alt="sml" class="prize-img" style="height: 80px;" /></a>
        </div>
      </div>
    </div>
  </div>