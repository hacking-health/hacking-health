<?php
$allpartner_link = get_post_meta( get_the_id(), 'wpcf-allpartner-link', true );
$partner_link = get_post_meta( get_the_id(), 'wpcf-partner-link', true );
?>
<div class="white-banner banner relative entry-content">
					<div class="bottom-arrow"></div>
					<div class="container">
<h2 class="h1" style="text-align: center;"><?php _e('Our partners', 'hacking'); ?></h2>
<div class="row">
  <?php if ($allpartner_link != '') { ?>
  <div class="col-md-6 text-center">
    <a href="<?php echo $allpartner_link; ?>" class="btn btn-tertiary btn-next"><?php _e('See all partners', 'hacking'); ?></a>
  </div>
  <?php } ?>
  <?php if ($partner_link != '') { ?>
  <div class="col-md-6 text-center">
    <a href="<?php echo $partner_link; ?>" class="btn btn-tertiary btn-next"><?php _e('Become a sponsor', 'hacking'); ?></a>
  </div>
  <?php } ?>
</div>
<div class="row platinum">
<h3 class="h2 bottom-spacing" style="text-align: center;"><?php _e('Platinum partners', 'hacking'); ?></h3>
<div class="platinum-col col-sm-4 col-sm-offset-0"><a href="http://www.sanofi.fr/l/fr/fr/index.jsp" target="_blank"><img class="aligncenter" src="/app/uploads/2015/09/sanofi2.jpg" alt="sanofi" /></a></div>
<div class="platinum-col col-sm-4 col-sm-offset-0"><a href="http://www.medtronic.com/us-en/index.html" target="_blank"><img class="aligncenter" src="/app/uploads/camp/partner/medtronic.jpg" alt="Medtronic" /></a></div>
<div class="platinum-col col-sm-4 col-sm-offset-0"><a href="http://europe-en-alsace.eu/" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://europe-en-alsace.eu/wp-content/uploads/2015/11/logo-UE-JPEG.jpg" alt="europe"height="96" /></a></div>
</div>
<div class="row gold">
<h3 class="h2 bottom-spacing" style="text-align: center;"><?php _e('Gold partners', 'hacking'); ?></h3>
<div class="gold-col col-sm-4 col-sm-offset-0"><a href="http://www.humanis.com/" target="_blank"><img class="size-full wp-image-1916 aligncenter" src="/app/uploads/2016/01/Humanis-logo-2012.png" alt="Humanis" width="1000" height="271" /></a>
</div><div class="gold-col col-sm-4 col-sm-offset-0"><a href="http://www3.gehealthcare.fr/" target="_blank"><img class="wp-image-2315 size-full aligncenter" src="/app/uploads/2014/04/GE-Healthcare.png" alt="" width="500" height="165" /></a>
</div><div class="gold-col col-sm-4 col-sm-offset-0"><a href="http://www.roche.fr/" target="_blank"><img class="aligncenter wp-image-2316 size-medium" src="/app/uploads/2014/04/roche-300x156.png" alt="roche" width="300" height="156" /></a>
</div></div>
<div class="row silver">
<h3 class="h2 bottom-spacing" style="text-align: center;"><?php _e('Silver partners', 'hacking'); ?></h3>
<div class="silver-col col-sm-4 col-sm-offset-0"><a href="http://www.strasbourg.eu/" target="_blank"><img class="size-silver-partner wp-image-1935 aligncenter" src="/app/uploads/2016/01/Strasbourg.eu-Signature-Verte-250x62.jpg" alt="Strasbourg.eu-Signature-Verte" width="250" height="62" /></a>
</div><div class="silver-col col-sm-4 col-sm-offset-0"><a href="http://www.servier.fr/" target="_blank"><img class="size-silver-partner wp-image-1525 aligncenter" src="/app/uploads/2015/12/servier2-250x80.png" alt="servier2" width="250" height="80" /></a>
</div></div>
<div class="row bronze">
<h3 class="h2 bottom-spacing" style="text-align: center;"><?php _e('Bronze partners', 'hacking'); ?></h3>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="https://www.vidal.fr/" target="_blank"><img class="size-full wp-image-1929 aligncenter" src="/app/uploads/2016/01/logo_vidal_beta1.png" alt="logo_vidal_beta" style="height: 90px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.medasys.fr" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="/app/uploads/2015/10/medasys.jpg" alt="medasys" style="height: 73px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.doctissimo.fr/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="/app/uploads/camp/partner/doctissimo.jpg" alt="doctissimo"  style="height: 50px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.mylan.fr/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="/app/uploads/camp/partner/mylan.png" alt="mylan"  style="height: 73px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.soprasteria.fr/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="/app/uploads/camp/partner/soprasteria.png" alt="soprasteria" style="height: 73px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.ihu-strasbourg.eu/ihu/" target="_blank"><img class="size-bronze-partner wp-image-677 aligncenter" src="/app/uploads/2015/11/ihu-200x64.jpg" alt="ihu" width="200" height="64" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.insimo.com/" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="/app/uploads/2016/01/INSIMO-LOGO-RECTANGLE-BGWHITE-200x61.png" alt="insimo" style="height: 73px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.icanopee.fr/index.php/fr/" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="/app/uploads/2016/01/icanop---200x72.png" alt="icanopee" style="height: 73px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.dataiku.com/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="/app/uploads/2016/01/Dataiku_Logo_HD1-200x73.png" alt="Dataiku" style="height: 73px;" /></a></div>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="https://www.creativedata.fr/" target="_blank"><img class="size-bronze-partner wp-image-1952 aligncenter" src="http://hackinghealth.ca//app/uploads/2014/04/saagie-red-500.png" alt="creative-data-saagie"  style="height: 120px;" /></a></div>
</div>
<div class="row bronze">
<h3 class="h2 bottom-spacing" style="text-align: center;">Hacking Health Factory <?php _e('partners', 'hacking'); ?></h3>
<div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.lajavaness.com/" target="_blank"><img class="size-bronze-partner wp-image-1932 aligncenter" src="/app/uploads/2016/01/logo_javaness.png" alt="la javaness" class="prize-img" /></a>
</div><div class="bronze-col col-sm-4 col-sm-offset-0"><a href="https://www.ekito.fr/" target="_blank"><img class="size-bronze-partner wp-image-1952 aligncenter" src="/app/uploads/2016/01/logo_ekito.png" alt="ekito" class="prize-img" /></a>
</div><div class="bronze-col col-sm-4 col-sm-offset-0"><a href="https://www.arena42.eu/" target="_blank"><img class="size-full wp-image-1929 aligncenter" src="/app/uploads/2015/10/logo-arena42.png" alt="arena42" class="prize-img" /></a>
</div><div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.semie-incal.com/" target="_blank"><img class="size-bronze-partner wp-image-1922 aligncenter" src="/app/uploads/2016/01/logo_semia.png" alt="semia" class="prize-img" /></a>
</div><div class="bronze-col col-sm-4 col-sm-offset-0"><a href="http://www.rockstart.com/accelerator/digitalhealth/" target="_blank"><img class="size-bronze-partner wp-image-1920 aligncenter" src="/app/uploads/2016/01/logo_rockstart.png" alt="rockstart" class="prize-img" /></a>
</div>
<div class="bronze-col col-sm-4 col-sm-offset-0">
	<div style="display: inline-block; margin-top: 20px;"><a href="http://www.businessangelssante.com/" target="_blank"><img class="size-bronze-partner wp-image-677 aligncenter" src="/app/uploads/2015/10/logo-angelssante.png" alt="angelssante" class="prize-img" style="height: 48px;"/></a></div>
	<div style="display: inline-block; margin-top: 20px;"><a href="http://www.ayming.fr/" target="_blank"><img class="size-bronze-partner wp-image-677 aligncenter" src="/app/uploads/camp/partner/ayming.jpg" alt="ayming" class="prize-img" style="height: 48px;"/></a></div>
	<div style="display: inline-block; margin-top: 20px;"><a href="http://www.degaullefleurance.com/" target="_blank"><img class="size-bronze-partner wp-image-677 aligncenter" src="/app/uploads/camp/partner/dgfla.png" alt="dgfla" class="prize-img" style="height: 48px;"/></a></div>
</div>
</div>
<div class="row simple">
<h3 class="h2 bottom-spacing" style="text-align: center;"><?php _e('Public Partners', 'hacking'); ?></h3>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://ch-troyes.fr/" target="_blank"><img class="size-medium wp-image-1909 aligncenter" src="/app/uploads/2016/01/LogoCHT-03.jpg" alt="LogoCHT-03" /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="https://www.alsace-esante.fr/" target="_blank"><img class="size-medium wp-image-679 aligncenter" src="/app/uploads/2015/11/alsace-esante.jpg" alt="alsace-esante"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.clinique-rhena.fr/fr" target="_blank"><img class="size-medium wp-image-672 aligncenter" src="/app/uploads/2015/09/rhena1.jpg" alt="rhena"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.ircad.fr/" target="_blank"><img class="size-medium wp-image-673 aligncenter" src="/app/uploads/2015/09/ircad1.jpg" alt="ircad"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.ghsv.org/" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://www.ghsv.org/wp-content/uploads/sites/2/2014/01/logo.png" alt="ghsv"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.chru-strasbourg.fr/" target="_blank"><img class="size-medium wp-image-675 aligncenter" src="/app/uploads/camp/partner/hus.jpg" alt="hus"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.chru-lille.fr/" target="_blank"><img class="size-medium wp-image-675 aligncenter" src="/app/uploads/camp/partner/chu-lille.gif" alt="chru lille"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://europe-en-alsace.eu/" target="_blank"><img class="size-medium wp-image-675 aligncenter" src="http://www.europe-en-alsace.eu/wp-content/uploads/2015/11/LEurope-sengage-en-Alsace-avec-le-FEDER_bleu.jpg" alt="feder" /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.alsace-biovalley.com/fr/" target="_blank"><img class="size-medium wp-image-674 aligncenter" src="http://bleger-rhein-poupon.com/wp-content/uploads/2015/01/biovalley_logo.jpg" alt="alsace biovalley"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.alsaceinnovation.eu/" target="_blank"><img class="size-medium wp-image-675 aligncenter" src="/app/uploads/camp/partner/alsace-innovation.jpg" alt="alsace innovation"  /></a></div>
<div class="simple-col col-sm-2 col-sm-offset-0"><a href="http://www.region.alsace/" target="_blank"><img class="size-medium wp-image-675 aligncenter" src="/app/uploads/camp/partner/region-alsace.jpg" alt="region alsace"  /></a></div>
</div>
</div>
</div>
