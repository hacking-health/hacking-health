<?php

$img_url = get_template_directory_uri() .'/img/';
$marker_sm_url = $marker_lg_url = $img_url .'location.png';

?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="/app/themes/hacking-health/js/map.js"></script>
<script type="text/javascript">venueMap('<?php echo $marker_lg_url; ?>', '<?php echo $marker_sm_url; ?>')</script>
<div id="map-canvas" class="google-maps contact-map"></div>
