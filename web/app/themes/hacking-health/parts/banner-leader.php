<?php
/**
 * The template for displaying the people's listing.
 *
 * @package Hacking Health
 * @since Hacking Health 1.0
 */

$event_tax_mentor_id = (int)get_post_meta( get_the_id(), 'wpcf-id-category-mentor-event', true );
$event_tax_jury_id = (int)get_post_meta( get_the_id(), 'wpcf-id-category-jury-event', true );
$event_tax_speaker_id = (int)get_post_meta( get_the_id(), 'wpcf-id-category-speaker-event', true );
$event_tax_organizer_id = (int)get_post_meta( get_the_id(), 'wpcf-id-category-organizer-event', true );

$mentor_args = array(
	'post_type'      => 'people',
	'posts_per_page' => -1,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-mentor-event',
			'field'    => 'id',
			'terms'    => $event_tax_mentor_id,
		)
	)
);

$mentor_query = new WP_Query( $mentor_args );
$results_mentor = $mentor_query->found_posts;

$jury_args = array(
	'post_type'      => 'people',
	'posts_per_page' => -1,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-jury-event',
			'field'    => 'id',
			'terms'    => $event_tax_jury_id,
		)
	)
);

$jury_query = new WP_Query( $jury_args );
$results_jury = $jury_query->found_posts;

$speaker_args = array(
	'post_type'      => 'people',
	'posts_per_page' => -1,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-speaker-event',
			'field'    => 'id',
			'terms'    => $event_tax_speaker_id,
		)
	)
);

$speaker_query = new WP_Query( $speaker_args );
$results_speaker = $speaker_query->found_posts;

$organizer_args = array(
	'post_type'      => 'people',
	'posts_per_page' => -1,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-organizer-event',
			'field'    => 'id',
			'terms'    => $event_tax_organizer_id,
		)
	)
);

$organizer_query = new WP_Query( $organizer_args );
$results_organizer = $organizer_query->found_posts;
?>

<?php if ( $mentor_query->have_posts() || $jury_query->have_posts() || $speaker_query->have_posts() || $organizer_query->have_posts() ) { ?>
<div class="banner-temoignage">
	<div class="bottom-arrow"></div>
	<div class="container">
		<?php if ( $mentor_query->have_posts() ) { ?>
		<div id="mentor-slider" class="entry-content row">
			<div class="col-md-12">
				<h2 class="h1 text-center"><?php _e('Mentors', 'hacking'); ?></h2>
				<ul class="slider">

					<?php //$i = 0; ?>

						<?php //if(($i % 6) == 0 || $i == 0) { ?>
							<li class="temoignage-item-slider">
								<div class="row">
						<?php //} ?>

						<?php while ( $mentor_query->have_posts() ) : $mentor_query->the_post(); ?>

						<?php $function = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-fonction-leader', true )); ?>
						<?php $description = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-description-leader', true ));
							  $description = trim(mb_substr(strip_tags($description),0,45)); ?>
						<?php $twitter = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-twitter-link', true )); ?>
						<?php $linkedin = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-linkedin-link', true )); ?>
						<?php $content = strip_tags(get_the_content()); ?>

								<div class="col-sm-6 col-md-4">
									<div class="item-temoignage">
										<?php echo get_the_post_thumbnail ( get_the_id(), 'thumbnail', array('class' => 'img-responsive img-temoignage') ); ?>
										<p class="function"><?php echo strip_tags($function); ?></p>
										<h3 class="title"><?php echo strip_tags(get_the_title()); ?></h3>
										<span class="hr"></span>
										<p class="description"><?php echo strip_tags($description); ?></p>
										<p class="content"><?php if(strlen(strip_tags(get_the_content())) > 154) { echo trim(mb_substr($content, 0, 154)).'...'; } else { echo $content; } ?></p>
										<div class="social-links">
											<?php if ($twitter != '') { ?>
												<a class="twitter" href="<?php echo strip_tags($twitter); ?>"><span class="icon-twitter"></span></a>
											<?php } ?>
											<?php if ($linkedin != '') { ?>
												<a class="linkedin" href="<?php echo strip_tags($linkedin); ?>"><span class="icon-linkedin"></span></a>
											<?php } ?>
										</div>
									</div>
								</div>

							<?php endwhile; // end of the loop. ?>

						<?php //$i ++; ?>

						<?php //if(($i % 6) == 0 || $results_mentor == $i ) { ?>
								</div>
							</li>
						<?php //} ?>

				</ul>
			</div>
		</div>
		<?php } ?>
		<?php if ( $jury_query->have_posts() ) { ?>
		<div id="jury-slider" class="entry-content row">
			<div class="col-md-12">
				<h2 class="h1 text-center"><?php _e('Jury', 'hacking'); ?></h2>
				<ul class="slider">

					<?php //$i = 0; ?>

						<?php //if(($i % 6) == 0 || $i == 0) { ?>
							<li class="temoignage-item-slider">
								<div class="row">
						<?php //} ?>

						<?php while ( $jury_query->have_posts() ) : $jury_query->the_post(); ?>

						<?php $function = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-fonction-leader', true )); ?>
						<?php $description = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-description-leader', true ));
							  $description = trim(mb_substr(strip_tags($description),0,45)); ?>
						<?php $twitter = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-twitter-link', true )); ?>
						<?php $linkedin = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-linkedin-link', true )); ?>
						<?php $content = strip_tags(get_the_content()); ?>

								<div class="col-sm-6 col-md-4">
									<div class="item-temoignage">
										<?php echo get_the_post_thumbnail ( get_the_id(), 'thumbnail', array('class' => 'img-responsive img-temoignage') ); ?>
										<p class="function"><?php echo strip_tags($function); ?></p>
										<h3 class="title"><?php echo strip_tags(get_the_title()); ?></h3>
										<span class="hr"></span>
										<p class="description"><?php echo strip_tags($description); ?></p>
										<p class="content"><?php if(strlen(strip_tags(get_the_content())) > 154) { echo trim(mb_substr($content, 0, 154)).'...'; } else { echo $content; } ?></p>
										<div class="social-links">
											<?php if ($twitter != '') { ?>
												<a class="twitter" href="<?php echo strip_tags($twitter); ?>"><span class="icon-twitter"></span></a>
											<?php } ?>
											<?php if ($linkedin != '') { ?>
												<a class="linkedin" href="<?php echo strip_tags($linkedin); ?>"><span class="icon-linkedin"></span></a>
											<?php } ?>
										</div>
									</div>
								</div>

							<?php endwhile; // end of the loop. ?>

						<?php //$i ++; ?>

						<?php //if(($i % 6) == 0 || $results_jury == $i ) { ?>
								</div>
							</li>
						<?php //} ?>

				</ul>
			</div>
		</div>
		<?php } ?>
		<?php if ( $speaker_query->have_posts() ) { ?>
		<div id="speaker-slider" class="entry-content row">
			<div class="col-md-12">
				<h2 class="h1 text-center"><?php _e('Speakers', 'hacking'); ?></h2>
				<ul class="slider">

					<?php //$i = 0; ?>

						<?php //if(($i % 6) == 0 || $i == 0) { ?>
							<li class="temoignage-item-slider">
								<div class="row">
						<?php //} ?>

						<?php while ( $speaker_query->have_posts() ) : $speaker_query->the_post(); ?>

						<?php $function = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-fonction-leader', true )); ?>
						<?php $description = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-description-leader', true ));
							  $description = trim(mb_substr(strip_tags($description),0,45)); ?>
						<?php $twitter = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-twitter-link', true )); ?>
						<?php $linkedin = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-linkedin-link', true )); ?>
						<?php $content = strip_tags(get_the_content()); ?>

								<div class="col-sm-6 col-md-4">
									<div class="item-temoignage">
										<?php echo get_the_post_thumbnail ( get_the_id(), 'thumbnail', array('class' => 'img-responsive img-temoignage') ); ?>
										<p class="function"><?php echo strip_tags($function); ?></p>
										<h3 class="title"><?php echo strip_tags(get_the_title()); ?></h3>
										<span class="hr"></span>
										<p class="description"><?php echo strip_tags($description); ?></p>
										<p class="content"><?php if(strlen(strip_tags(get_the_content())) > 154) { echo trim(mb_substr($content, 0, 154)).'...'; } else { echo $content; } ?></p>
										<div class="social-links">
											<?php if ($twitter != '') { ?>
												<a class="twitter" href="<?php echo strip_tags($twitter); ?>"><span class="icon-twitter"></span></a>
											<?php } ?>
											<?php if ($linkedin != '') { ?>
												<a class="linkedin" href="<?php echo strip_tags($linkedin); ?>"><span class="icon-linkedin"></span></a>
											<?php } ?>
										</div>
									</div>
								</div>

							<?php endwhile; // end of the loop. ?>

						<?php //$i ++; ?>

						<?php //if(($i % 6) == 0 || $results_speaker == $i ) { ?>
								</div>
							</li>
						<?php //} ?>

				</ul>
			</div>
		</div>
		<?php } ?>
		<?php if ( $organizer_query->have_posts() ) { ?>
		<div id="organizer-slider" class="entry-content row">
			<div class="col-md-12">
				<h2 class="h1 text-center"><?php _e('Organizers', 'hacking'); ?></h2>
				<ul class="slider">

					<?php //$i = 0; ?>

						<?php //if(($i % 6) == 0 || $i == 0) { ?>
							<li class="temoignage-item-slider">
								<div class="row">
						<?php //} ?>

						<?php while ( $organizer_query->have_posts() ) : $organizer_query->the_post(); ?>

						<?php $function = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-fonction-leader', true )); ?>
						<?php $description = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-description-leader', true ));
							  $description = trim(mb_substr(strip_tags($description),0,45)); ?>
						<?php $twitter = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-twitter-link', true )); ?>
						<?php $linkedin = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-linkedin-link', true )); ?>
						<?php $content = strip_tags(get_the_content()); ?>

								<div class="col-sm-6 col-md-4">
									<div class="item-temoignage">
										<?php echo get_the_post_thumbnail ( get_the_id(), 'thumbnail', array('class' => 'img-responsive img-temoignage') ); ?>
										<p class="function"><?php echo strip_tags($function); ?></p>
										<h3 class="title"><?php echo strip_tags(get_the_title()); ?></h3>
										<span class="hr"></span>
										<p class="description"><?php echo strip_tags($description); ?></p>
										<p class="content"><?php if(strlen(strip_tags(get_the_content())) > 154) { echo trim(mb_substr($content, 0, 154)).'...'; } else { echo $content; } ?></p>
										<div class="social-links">
											<?php if ($twitter != '') { ?>
												<a class="twitter" href="<?php echo strip_tags($twitter); ?>"><span class="icon-twitter"></span></a>
											<?php } ?>
											<?php if ($linkedin != '') { ?>
												<a class="linkedin" href="<?php echo strip_tags($linkedin); ?>"><span class="icon-linkedin"></span></a>
											<?php } ?>
										</div>
									</div>
								</div>

							<?php endwhile; // end of the loop. ?>

						<?php //$i ++; ?>

						<?php //if(($i % 6) == 0 || $results_organizer == $i ) { ?>
								</div>
							</li>
						<?php //} ?>

				</ul>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<?php //do_action( 'bxslider', 'mentor-slider'); ?>
<?php //do_action( 'bxslider', 'jury-slider'); ?>
<?php //do_action( 'bxslider', 'speaker-slider'); ?>
<?php //do_action( 'bxslider', 'organizer-slider'); ?>

<?php } ?>
