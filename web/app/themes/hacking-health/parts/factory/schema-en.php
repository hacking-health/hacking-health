<div class="factory-schema">
  <div class="part1">
    <div>
      <div class="schema-block">
        <span class="icon-lamp"></span>
        <h2>IDEAS</h2>
      </div>
      <div class="arrow">
        <span class="icon-arrow-right"></span>
      </div>
    </div>
    <div>
      <div class="schema-block">
        <span class="icon-projects"></span>
        <h2>PROJECTS</h2>
      </div>
      <div class="arrow">
        <span class="icon-arrow-right"></span>
      </div>
    </div>
  </div>
  <div class="part2">
    <div>
      <div class="schema-block linkable">
        <span class="icon-education"></span>
        <h2><a href="/factory-education/">EDUCATION</a></h2>
        <p>training center in health systems, health technologies, entrepreneurship, design, legal ... with live immersion sessions </p>
      </div>
      <div>
        <div class="line"></div>
      </div>
      <div class="schema-block linkable">
        <span class="icon-studio"></span>
        <h2><a href="/factory-studio/">STUDIO</a></h2>
        <p>task force teams of developers, designers, entrepreneurs, experts ...</p>
      </div>
    </div>
    <div>
      <div class="vertical-alignment-top">
        <div class="vertical-line-left"></div>
      </div>
      <div class="schema-block linkable">
        <span class="icon-sandbox"></span>
        <h2><a href="/factory-experimentation-platform/">SANDBOX</a></h2>
        <p>experimentation platform and network of partners to test and validate your ideas</p>
      </div>
      <div class="vertical-alignment-top">
        <div class="vertical-line-right"></div>
      </div>
    </div>
    <div>
    <div class="schema-footer">
    <div class="arrow">
      <span class="icon-arrow-top"></span>
    </div>
    <div class="schema-block">
      <span class="icon-money-bag"></span>
      <h2>FUND</h2>
    </div>
    </div>
    </div>
  </div>
  <div class="part3">
    <div class="arrow">
      <span class="icon-arrow-right"></span>
    </div>
    <div class="schema-block">
      <span class="icon-rocket"></span>
      <h2>STARTUPS</h2>
    </div>
  </div>
</div>
