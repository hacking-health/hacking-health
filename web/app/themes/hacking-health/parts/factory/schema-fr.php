<div class="factory-schema">
  <div class="part1">
    <div>
      <div class="schema-block">
        <span class="icon-lamp"></span>
        <h2>IDÉES</h2>
      </div>
      <div class="arrow">
        <span class="icon-arrow-right"></span>
      </div>
    </div>
    <div>
      <div class="schema-block">
        <span class="icon-projects"></span>
        <h2>PROJETS</h2>
      </div>
      <div class="arrow">
        <span class="icon-arrow-right"></span>
      </div>
    </div>
  </div>
  <div class="part2">
    <div>
      <div class="schema-block linkable">
        <span class="icon-education"></span>
        <h2><a href="/factory-education/">FORMATION</a></h2>
        <p>des cours sur les systèmes de santé, les technologies, l'entreprenariat, le design, les réglementations ... avec sessions d'immersion sur le terrain</p>
      </div>
      <div>
        <div class="line"></div>
      </div>
      <div class="schema-block linkable">
        <span class="icon-studio"></span>
        <h2><a href="/factory-studio/">STUDIO</a></h2>
        <p>une équipe sur mesure de développeurs, designers, entrepreneurs, experts du domaine de la santé avec un accompagnement agile des projets</p>
      </div>
    </div>
    <div>
      <div class="vertical-alignment-top">
        <div class="vertical-line-left"></div>
      </div>
      <div class="schema-block linkable">
        <span class="icon-sandbox"></span>
        <h2><a href="/factory-experimentation-platform/">EXPÉRIMENTATION</a></h2>
        <p>une plateforme d'expérimentation et un réseau de partenaires pour tester et valider les idées</p>
      </div>
      <div class="vertical-alignment-top">
        <div class="vertical-line-right"></div>
      </div>
    </div>
    <div>
    <div class="schema-footer">
    <div class="arrow">
      <span class="icon-arrow-top"></span>
    </div>
    <div class="schema-block">
      <span class="icon-money-bag"></span>
      <h2>FOND D'INVESTISSEMENTS</h2>
    </div>
    </div>
    </div>
  </div>
  <div class="part3">
    <div class="arrow">
      <span class="icon-arrow-right"></span>
    </div>
    <div class="schema-block">
      <span class="icon-rocket"></span>
      <h2>STARTUPS</h2>
    </div>
  </div>
</div>
