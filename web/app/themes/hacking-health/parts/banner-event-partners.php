<?php
$partner_link = get_post_meta( get_the_id(), 'wpcf-partner-link', true );
$allpartner_link = get_post_meta( get_the_id(), 'wpcf-allpartner-link', true );
?>
<div class="banner-partners">
  <div class="bottom-arrow"></div>
  <div class="container">
    <div class="entry-content">
      <h2 class="h1 text-center"><?php _e('Our partners', 'hacking'); ?></h2>

        <?php wp_reset_query(); ?>
        <?php get_template_part( 'content', 'city-partners-event' );?>

      <?php if ($partner_link != '' || $allpartner_link != '') { ?>
      <div class="row">
        <div class="col-md-6 text-center">
          <a href="<?php echo $allpartner_link; ?>" class="btn btn-primary btn-next"><?php _e('See all partners', 'hacking'); ?></a>
        </div>
        <div class="col-md-6 text-center">
          <a href="<?php echo $partner_link; ?>" class="btn btn-primary btn-next"><?php _e('Become a sponsor', 'hacking'); ?></a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
