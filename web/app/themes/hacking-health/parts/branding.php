<?php

$site_title_tag = is_front_page() ? 'h1' : 'div';
$site_logo_title = is_front_page() ? '' : __(" - Retour à l'accueil", 'hacking');

$primary_nav = $primary_nav_mobile = wp_nav_menu( array(
	'theme_location'	=> 'primary',
	'container'			=> false,
	'menu_class'		=> '',
	'items_wrap'		=> '<ul class="relative nav main-nav nav-justified">%3$s</ul>',
	'depth'				=> 2,
	'walker'			=> new GMM_Walker_Nav_Menu(),
	'echo'				=> false,
) );

$city_args = array (
	'post_type'      => array( 'city' ),
	'posts_per_page' => -1,
	'order'          => 'ASC',
	'orderby'        => 'title',
);

$city_query = new WP_Query( $city_args );
$select_city_options = array();

if ( count($city_query->posts) ) {
	while ( $city_query->have_posts() ) : $city_query->the_post();
		$select_city_options[] = array(
			'id'   => get_the_ID(),
			'text' => html_entity_decode(get_the_title()),
			'url'  => get_the_permalink(),
		);
	endwhile;
}

wp_reset_query();

$language_nav = '';
if ( function_exists('pll_the_languages') ) {
	$language_nav = pll_the_languages( array('echo' => false) );
}

?>

	<header class="branding">
		<div class="hidden-lg hidden-md mobile-menu">
			<div class="container">
				<div class="row">
					<a class="col-xs-9 site-logo" href="http://hackinghealth.camp" title="<?php echo esc_attr( get_bloginfo( 'name' ) ) . $site_logo_title; ?>">
						<img class="visible-sm-inline visible-xs-inline img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/hacking-health.png" alt="<?php bloginfo( 'name' ); ?>" />
					</a>
					<div class="mobile-actions col-xs-3">
						<button class="mobile-button collapsed ico-menu pull-right" data-toggle="collapse" data-target=".header-mobile-navbar" data-parent=".mobile-menu"><span class="line-menu"></span><span class="line-menu"></span><span class="line-menu"></span></button>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar-collapse collapse header-mobile-navbar">
			<nav role="navigation" class="navbar-default site-navigation main-navigation hidden-md hidden-lg">
				<?php echo $primary_nav_mobile; ?>
			</nav>
		</div>
		<div class="navbar-collapse collapse header-navbar">
			<div class="branding-top">
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<a href="http://hackinghealth.camp"><img src="/app/uploads/logo-hacking-health.png" height="50"></a>
						</div>
						<div class="col-lg-offset-1 col-lg-4 col-md-offset-0 col-md-4 select-city-container">
						<!--<?php if ( count($select_city_options) ) : ?>
							<select id="select-city" class="select2-default" data-placeholder="<?php _e( 'Select your city', 'hacking' ); ?>">
								<option></option>
							</select>
							<script>
							var select_cites_options = <?php echo json_encode($select_city_options) ?>;
							</script>
						<?php endif; ?>-->
						</div>
						<div class="col-md-3 col-lg-2 custom-margin text-right">
							<div class="switch-languages list-inline"><?php echo $language_nav; ?></div>
						</div>
					</div>
				</div>
			</div>
			<?php if((get_post_type() != 'event') && (get_post_type() != 'city') && (get_post_type() != 'post') && (get_post_type() != 'success-story') ) {?>
				<nav id="access" role="navigation" class="navbar-default site-navigation main-navigation">
					<div class="container">
						<?php echo $primary_nav; ?>
					</div>
				</nav>
			<?php } ?>
		</div>
	</header><!-- #branding -->
