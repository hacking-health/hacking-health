<?php
/**
 * The template for displaying the news listing.
 *
 * @package Hacking Health
 * @since Hacking Health 1.0
 */

$args = array (
	'post_type'              => 'post',
	'post_status'            => 'publish',
	'order'                  => 'DESC',
	'posts_per_page'         => 3,
);
// The Queries
$query = new WP_Query( $args );
?>

<div class="banner-news">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div class="entry-content">
			<?php if ( $query->have_posts() ) { ?>
			
			<div class="row">
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<div class="col-md-4">
						<div class="item-news bottom-spacing">
							<?php echo get_the_post_thumbnail( get_the_id(), 'img-news', array('class' => 'img-responsive img-temoignage center-block') ); ?>
							<div class="info-news clearfix">
								<p class="date"><?php echo strip_tags(gmm_posted_on()); ?></p>
								<p class="title"><?php echo mb_substr(strip_tags(get_the_title()), 0, 100).'...'; ?></p>
								<a href="<?php echo get_the_permalink(); ?>" class="text-right more-info"><span class="icon-plus"></span></a>
							</div>
						</div>
					</div>
				<?php endwhile; // end of the loop. ?>
			</div>
			<div class="row">
				<div class="col-md-offset-4 col-md-4 top-spacing">
					<a href="<?php echo gmm_get_post_archive_link(); ?>" class="btn btn-secondary btn-next btn-block"><?php _e( 'View all news !', 'hacking' ); ?></a>
				</div>
			</div>
			
			<?php } ?>
		</div>
	</div>
</div>