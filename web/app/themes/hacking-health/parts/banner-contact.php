<div class="banner-contact">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div class="entry-content">
			<h2 class="h1 text-center"><?php _e('Contact us', 'hacking'); ?></h2>
			
			<?php if (get_bloginfo('language') == "en-GB") {
				echo do_shortcode( getenv('WP_SHORTCODE_CONTACT_EN') );
			} elseif (get_bloginfo('language') == "fr-FR") {
				echo do_shortcode( getenv('WP_SHORTCODE_CONTACT_FR') );
			}?>
		
		</div>
	</div>
</div>