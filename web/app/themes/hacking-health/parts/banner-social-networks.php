<div class="banner-social-networks">
	<div class="bottom-arrow"></div>
	<div class="container">
	  <?php echo do_shortcode('[row][col class="top-spacing" offset_md="1" md="5" sm="6" xs="12"][hh-facebook-block width="458" height="230"][/col][col class="top-spacing" xs="12" sm="6" md="5"][hh-twitter-block width="458" height="230"][/col][/row]'); ?>
	</div>
</div>
