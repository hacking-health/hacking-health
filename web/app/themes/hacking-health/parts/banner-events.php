<?php
/**
 * The template for displaying the people's listing.
 *
 * @package Hacking Health
 * @since Hacking Health 1.0
 */

$args = array (
	'post_type'      => 'event',
	'posts_per_page' => -1,
	'orderby'        => 'wpcf-event-date',
	'order'          => 'ASC',
	'meta_key'       => 'wpcf-event-date'
);
// The Queries
$query = new WP_Query( $args );

if ( $query->have_posts() ) :
?>

<div class="banner-event">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div id="event-slider" class="entry-content row">
			<div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
				<div class="row bottom-spacing">
					<div class="col-md-offset-4 col-md-4">
						<div class="btn-event"><?php _e('Upcoming Events', 'hacking'); ?></div>
					</div>
				</div>
				<ul class="slider event-slider">

					<?php while ( $query->have_posts() ) : $query->the_post(); ?>

						<?php

						$url = get_post_meta( get_the_id(), 'wpcf-eventbrite-link', true );
						$event_date = get_post_meta( get_the_id(), 'wpcf-event-date', true );
						$event_end_date = get_post_meta( get_the_id(), 'wpcf-event-end-date', true );
						$event_hour = get_post_meta( get_the_id(), 'wpcf-event-hour', true );

						if ( trim($event_date) ) {
							$date_format = gmm_current_language() == 'fr' ? 'l j F Y' : 'l F j Y';
							$event_date_final = date_i18n($date_format, $event_date);
						}

						if ( trim($event_end_date) ) {
							$date_format = gmm_current_language() == 'fr' ? 'l j F Y' : 'l F j Y';
							$event_end_date_final = date_i18n($date_format, $event_end_date);
						}

						?>

						<?php if( (int)$event_date+86400 >= time() || (int)$event_end_date+86400 >= time() ) { ?>

							<li class="item-event text-center">
								<p class="title"><?php echo get_the_title(); ?></p>
								<p class="date">
									<span class="icon-calendar-insert"></span>
									<?php echo $event_date_final;?>
									<?php if ($event_end_date != null) {
										echo ' - ' . $event_end_date_final;
									} ?>
									<?php if ($event_hour != null) {
										echo ' - ' . $event_hour;
									} ?>
								</p>
								<div class="row top-spacing">
									<div class="col-md-offset-2 col-md-4 col-sm-offset-2 col-sm-8 half-bottom-spacing">
										<a target="_blank" href="<?php echo $url; ?>" class="btn btn-tertiary btn-block btn-next"><?php _e('Register Now!', 'hacking'); ?></a>
									</div>
									<div class="col-md-offset-0 col-md-4 col-sm-offset-2 col-sm-8">
										<a href="<?php echo get_permalink(); ?>" class="btn btn-quaternary btn-block btn-next"><?php _e('See more', 'hacking'); ?></a>
									</div>
								</div>
							</li>

						<?php } ?>

					<?php endwhile; // end of the loop. ?>

				</ul>
			</div>
		</div>
	</div>
</div>
<div class="banner-news">
	<div class="bottom-arrow"></div>
	<div class="container">
		<div class="entry-content">
			<div class="row">
				<div class="col-md-offset-4 col-md-4">
					<a href="<?php echo get_post_type_archive_link('event'); ?>" class="btn btn-secondary btn-block btn-next"><?php _e('See all the events!', 'hacking'); ?></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	do_action( 'bxslider', 'event-slider');
	endif;
?>
