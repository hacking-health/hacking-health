<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */
?>

<article id="post-0" class="post no-results not-found">
	<header class="entry-header">
		<h1 class="entry-title"><?php _e( "Oups, rien n'a été trouvé", 'hacking' ); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( is_home() ) : ?>

			<p class="alert alert-block"><?php printf( __( 'Aucun contenu.', 'hacking' ), admin_url( 'post-new.php' ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p class="alert alert-block"><?php _e( "Aucun contenu n'a été trouvé. Essayez une recherche avec d'autres mot-clés.", 'hacking' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p class="alert alert-block"><?php _e( "Aucun contenu n'a été trouvé. Essayez d'effectuer une recherche", 'hacking' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .entry-content -->
</article><!-- #post-0 .post .no-results .not-found -->
