<?php
/**
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<h2 class="h1 entry-title"><?php the_title(); ?></h2>
		
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	</div>
</article>
