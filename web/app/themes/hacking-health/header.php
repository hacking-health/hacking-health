<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

get_template_part('parts/head');
?>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T45J9G2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	<?php do_action( 'before' ); ?>
	
	<?php get_template_part('parts/branding'); ?>
	
	<?php
		if ( is_front_page() ) {
			get_template_part('parts/slider', 'home');
		}
	?>
	
	<div id="main" class="clearfix site-main">
