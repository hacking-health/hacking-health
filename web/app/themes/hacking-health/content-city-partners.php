<?php
/**
 * The template for displaying the partners in a city page.
 *
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$partner_url = get_post_meta( get_the_id(), 'wpcf-partner-url', true );

?>

<div class="col-md-4">
	<a href="<?php echo $partner_url; ?>" target="_blank">
		<?php echo get_the_post_thumbnail ( get_the_id(), 'full', array('class' => 'img-responsive bottom-spacing', 'alt' => get_the_title()) ); ?>
	</a>
</div>
