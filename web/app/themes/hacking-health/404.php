<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<article id="post-0" class="post error404 not-found">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( "La page que vous avez demandée n'existe pas, ou elle n'est plus accessible à cette adresse", 'hacking' ); ?></h1>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<p><?php _e( "Pour nous signaler une erreur, n'hésitez pas à nous ", 'hacking' ); ?><a href="mailto:contact@hackinghealth.ca"><?php _e( "contacter", 'hacking' ); ?></a>.</p>
					<p><?php _e( "Veuillez nous excuser pour la gêne occasionnée.", 'hacking' ); ?></p>
				</div><!-- .entry-content -->
			</article><!-- #post-0 .post .error404 .not-found -->

		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->

<?php get_footer(); ?>
