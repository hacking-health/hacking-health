<?php
/**
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$args = array(
	'post_type'      => 'event',
	'posts_per_page' => -1,
	'orderby'        => 'wpcf-event-date',
	'order'          => 'ASC',
	'meta_key'       => 'wpcf-event-date'
);

$query = new WP_Query( $args );

if ( @$_GET['past-events'] === null) {
	$past_events = false;	
} else {
	$past_events = true;
}

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<div class="entry-content">

					<div class="img-banner archive-event-banner">
						<div class="container">
							<div class="text-center">
								<p class="title"><?php _e('Hacking Health Events', 'hacking'); ?></p>
								<p class="baseline"></p>
							</div>
						</div>
					</div>
					<header class="entry-header">
						<h1 class="entry-title"><?php _e('All the events', 'hacking'); ?></h1>
					</header><!-- .entry-header -->

					<div class="container">

					<?php if ( $past_events == true ) { ?>

						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							
							<?php $event_date = get_post_meta( get_the_id(), 'wpcf-event-date', true ); ?>							

							<?php if( (int)$event_date < time() ) { ?>
								
								<?php get_template_part( 'content', 'archive-event' ); ?>

							<?php } ?>
						<?php endwhile; // end of the loop. ?>

					<?php } else { ?>

						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							<?php $event_date = get_post_meta( get_the_id(), 'wpcf-event-date', true ); ?>							

							<?php if( (int)$event_date >= time() ) { ?>

								<?php get_template_part( 'content', 'archive-event' ); ?>

							<?php } ?>
						<?php endwhile; // end of the loop. ?>

					<?php } ?>

					</div>

					<?php if ( $past_events === false ) { ?>
						<div class="banner-news">
							<div class="bottom-arrow"></div>
							<div class="container">
								<div class="entry-content">
									<div class="row">
										<div class="col-md-offset-4 col-md-4">
											<a href="?past-events" class="btn btn-secondary btn-block btn-next">See all the past events!</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php get_footer(); ?>
