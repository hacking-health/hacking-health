<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package gms
 * @since gms 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<?php get_template_part( 'parts/banner-video' ); ?>
			<?php get_template_part( 'parts/banner-events' ); ?>
			<?php wp_reset_query(); ?>
			<div class="banner-numbers">
				<div class="bottom-arrow"></div>
				<div class="container">
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php get_template_part( 'parts/banner-social-networks' ); ?>
			<?php get_template_part( 'parts/banner-partners' ); ?>
			<?php get_template_part( 'parts/banner-contact' ); ?>
		</div><!-- #primary .content-area -->

<?php get_footer(); ?>
