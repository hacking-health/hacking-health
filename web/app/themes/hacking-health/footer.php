<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$primary_nav = $primary_nav_mobile = wp_nav_menu( array(
	'theme_location'	=> 'primary',
	'container'			=> false,
	'menu_class'		=> '',
	'items_wrap'		=> '<ul class="relative nav main-nav nav-justified top-spacing">%3$s</ul>',
	'depth'				=> 2,
	'walker'			=> new GMM_Walker_Nav_Menu(),
	'echo'				=> false,
) );

?>

	</div><!-- #main .site-main -->

	<footer class="footer" role="contentinfo">
		<div class="container">
			<!--<?php echo $primary_nav; ?>
			<hr>-->
			<div class="row">
				<div class="col-md-4 col-lg-3">
					<div class="social-buttons text-center top-spacing bottom-spacing">
						<a target="_blank" class="facebook" href="https://www.facebook.com/hackinghealthcamp"><span class="icon-facebook"></span></a>
						<a target="_blank" class="twitter" href="https://twitter.com/hackinghealtheu"><span class="icon-twitter"></span></a>
						<a target="_blank" class="linkedin" href="https://www.linkedin.com/groups/6400524"><span class="icon-linkedin"></span></a>
						<a target="_blank" class="youtube" href="https://www.youtube.com/c/hackinghealthcamp"><span class="icon-youtube-play"></span></a>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-7 col-lg-offset-3 col-lg-6">
					<div class="footer-links top-spacing">
						<a target="_blank" rel="nofollow" href="http://goodmorningmajor.com/"><?php _e('Web agency', 'hacking'); ?></a><a href="/privacy-policy"><?php _e('Privacy policy', 'hacking'); ?></a><a href="#"><?php _e('Terms of use', 'hacking'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>
