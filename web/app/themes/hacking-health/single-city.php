<?php
/**
 * The Template for displaying all single posts.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$hashtag = get_post_meta( get_the_id(), 'wpcf-hashtag', true );
$city_tax_event_id = pll_get_term((int)get_post_meta( get_the_id(), 'wpcf-id-category-event-city', true ));
$city_tax_story_id = pll_get_term((int)get_post_meta( get_the_id(), 'wpcf-id-category-story-city', true ));
$city_tax_post_id = pll_get_term((int)get_post_meta( get_the_id(), 'wpcf-id-category-post-city', true ));
$city_tax_leader_id = pll_get_term((int)get_post_meta( get_the_id(), 'wpcf-id-category-people-city', true ));
$mailchimp_url = get_post_meta( get_the_id(), 'wpcf-mailchimp-url', true );
$external_website = get_post_meta( get_the_id(), 'wpcf-external-website', true );
$contact_form = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-contact-form', true ));
$sponsors = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-sponsors', true ));

$event_args = array(
	'post_type'      => 'event',
	'posts_per_page' => 4,
	'orderby'        => 'wpcf-event-date',
	'order'          => 'ASC',
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-event-city',
			'field'    => 'id',
			'terms'    => $city_tax_event_id,
		)
	),
	'meta_key' => 'wpcf-event-date',
	'meta_query' => array(
		array(
			'key'     => 'wpcf-event-date',
			'value'   => time()-86400,
			'compare' => '>='
		),
	),
);

$event_query = new WP_Query( $event_args );

$story_args = array(
	'post_type'      => 'success-story',
	'posts_per_page' => 2,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-story-city',
			'field'    => 'id',
			'terms'    => $city_tax_story_id,
		)
	)
);

$story_query = new WP_Query( $story_args );

$post_args = array(
	'post_type'      => 'post',
	'posts_per_page' => 3,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-post-city',
			'field'    => 'id',
			'terms'    => $city_tax_post_id,
		)
	)
);

$post_query = new WP_Query( $post_args );

$leader_args = array(
	'post_type'      => 'people',
	'posts_per_page' => -1,
	'tax_query'      => array(
		array(
			'taxonomy' => 'category-leader-city',
			'field'    => 'id',
			'terms'    => $city_tax_leader_id,
		)
	)
);

$leader_query = new WP_Query( $leader_args );
$results = $leader_query->found_posts;

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<div class="single-city-banner" style="background-image: url('<?php echo $url; ?>')">
					<div class="container">
						<div class="text-center">
							<h1 class="title"><?php echo get_the_title(); ?></h1>
							<a target="_blank" href="http://www.twitter.com/<?php echo $hashtag; ?>" class="hashtag"><?php echo $hashtag; ?></a>
						</div>
						<?php if($mailchimp_url != '') { ?>
						<div class="row">
							<div class="col-md-offset-3 col-md-7 col-lg-6">
								<div class="mailchimp-box">
									<p class="text-center"><?php _e('Join the movement', 'hacking') ; ?></p>
									<form class="mailchimp" action="<?php echo $mailchimp_url; ?>" method="post" target="_blank">
										<div class="form-group row">
											<div class="col-md-8">
												<input class="form-control" name="EMAIL" type="email" placeholder="<?php _e('Enter your e-mail', 'hacking'); ?>">
											</div>
											<div class="col-md-4">
												<button type="submit" class="btn btn-primary btn-block btn-next"><?php _e('Join!', 'hacking'); ?></button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<?php } else if ($external_website != '') { ?>
							<div class="row">
								<div class="col-md-offset-3 col-md-7 col-lg-6">
									<div class="mailchimp-box">
										<a class="btn btn-primary btn-block btn-next" href="<?php echo $external_website; ?>"><?php _e('Visit our page', 'hacking') ; ?></a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php if( get_the_content() != "") { ?>
					<?php the_content(); ?>
				<?php } ?>
				<?php if(!is_wp_error(get_term_link($city_tax_event_id, 'category-event-city' ))) { ?>
				<div class="banner-events">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">

							<h2 class="h1"><?php _e('Upcoming Events', 'hacking'); ?></h2>

							<?php while ( $event_query->have_posts() ) : $event_query->the_post(); ?>

								<?php $event_date = (int)get_post_meta( get_the_id(), 'wpcf-event-date', true ); ?>

                                <?php $event_end_date = (int)get_post_meta( get_the_id(), 'wpcf-event-end-date', true ); ?>

								<?php if( $event_date+86400 >= time() || $event_end_date+86400 >= time() ) { ?>

									<?php get_template_part( 'content', 'archive-event' );?>

								<?php } ?>

							<?php endwhile; // end of the loop. ?>

							<div class="row">
								<div class="col-md-offset-4 col-md-4">
									<a href="<?php echo get_term_link($city_tax_event_id, 'category-event-city' ); ?>" class="btn btn-primary btn-block btn-next"><?php _e('View all events', 'hacking'); ?></a>
								</div>
							</div>

							<?php wp_reset_query(); ?>

						</div>
					</div>
				</div>
				<div class="banner-story">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">
							<div class="row">
								<div class="col-md-offset-4 col-md-4">
									<a rel="nofollow" href="<?php echo get_term_link($city_tax_event_id, 'category-event-city' ); ?>?past-events" class="btn btn-primary btn-block btn-next"><?php _e('See the past events!', 'hacking'); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if(!is_wp_error(get_term_link($city_tax_post_id, 'category-post-city' ))) { ?>
				<div class="banner-news">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">

							<div class="row">

								<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>

									<?php get_template_part( 'content', 'city-post' );?>

								<?php endwhile; // end of the loop. ?>

								<?php wp_reset_query(); ?>

							</div>

							<div class="row">
								<div class="col-md-offset-4 col-md-4 top-spacing">
									<a href="<?php echo get_term_link($city_tax_post_id, 'category-post-city' ); ?>" class="btn btn-secondary btn-next btn-block"><?php _e( 'View all news!', 'hacking' ); ?></a>
								</div>
							</div>

						</div>
					</div>
				</div>
				<?php } ?>
				<?php if(!is_wp_error(get_term_link($city_tax_story_id, 'category-story-city' ))) { ?>
				<div class="banner-story">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">

							<h2 class="h1"><?php _e('Success stories', 'hacking'); ?></h2>

							<?php while ( $story_query->have_posts() ) : $story_query->the_post(); ?>

								<?php get_template_part( 'content', 'archive-success-story' );?>

							<?php endwhile; // end of the loop. ?>

							<?php wp_reset_query(); ?>

							<div class="row">
								<div class="col-md-offset-4 col-md-4 top-spacing">
									<a href="<?php echo get_term_link($city_tax_story_id, 'category-story-city' ); ?>" class="btn btn-primary btn-next btn-block"><?php _e( 'View all success stories!', 'hacking' ); ?></a>
								</div>
							</div>

						</div>
					</div>
				</div>
				<?php } ?>
				<?php if ( $leader_query->have_posts() ) { ?>
				<div class="banner-temoignage">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div id="temoignage-slider" class="entry-content row">
							<div class="col-md-offset-2 col-md-8">
								<h2 class="h1 text-center"><?php _e('Leaders', 'hacking'); ?></h2>
								<ul class="slider">

									<?php $i = 0; ?>

									<?php while ( $leader_query->have_posts() ) : $leader_query->the_post(); ?>

										<?php if(($i % 4) == 0 || $i == 0) { ?>
											<li class="temoignage-item-slider">
												<div class="row">
										<?php } ?>

										<?php $function = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-fonction-leader', true )); ?>
										<?php $description = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-description-leader', true ));
											  $description = trim(mb_substr(strip_tags($description),0,45)); ?>
										<?php $twitter = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-twitter-link', true )); ?>
										<?php $linkedin = apply_filters( 'the_content', get_post_meta( get_the_id(), 'wpcf-linkedin-link', true )); ?>
										<?php $content = strip_tags(get_the_content()); ?>

												<div class="col-sm-6">
													<div class="item-temoignage">
														<?php echo get_the_post_thumbnail ( get_the_id(), 'thumbnail', array('class' => 'img-responsive img-temoignage') ); ?>
														<p class="function"><?php echo strip_tags($function); ?></p>
														<h3 class="title"><?php echo strip_tags(get_the_title()); ?></h3>
														<span class="hr"></span>
														<p class="description"><?php echo strip_tags($description); ?></p>
														<p class="content"><?php if(strlen(strip_tags(get_the_content())) > 154) { echo trim(mb_substr($content, 0, 154)).'...'; } else { echo $content; } ?></p>
														<div class="social-links">
															<?php if ($twitter != '') { ?>
																<a class="twitter" href="<?php echo strip_tags($twitter); ?>"><span class="icon-twitter"></span></a>
															<?php } ?>
															<?php if ($linkedin != '') { ?>
																<a class="linkedin" href="<?php echo strip_tags($linkedin); ?>"><span class="icon-linkedin"></span></a>
															<?php } ?>
														</div>
													</div>
												</div>

										<?php $i ++; ?>

										<?php if(($i % 4) == 0 || $results == $i ) { ?>
												</div>
											</li>
										<?php } ?>

									<?php endwhile; // end of the loop. ?>

								<?php wp_reset_query(); ?>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if($sponsors != '') { ?>
					<?php echo $sponsors; ?>
				<?php } ?>
				<div class="video-city relative">
					<?php get_template_part( 'parts/banner-video' ); ?>
				</div>

								<?php wp_reset_query(); ?>
								<?php get_template_part( 'content', 'city-partners-city' );?>

				<?php
				// Formulaire de contact
				?>
				<?php if ($contact_form != '') { ?>
				<div class="banner-contact">
					<div class="bottom-arrow"></div>
					<div class="container">
						<div class="entry-content">
							<h2 class="h1 text-center"><?php _e('Contact us', 'hacking'); ?></h2>
							<?php echo $contact_form; ?>
						</div>
					</div>
				</div>
				<?php } else { ?>
					<?php get_template_part( 'parts/banner-contact' ); ?>
				<?php } ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php do_action( 'bxslider', 'temoignage-slider'); ?>
<?php get_footer(); ?>
