<?php
/**
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="thumbnail">
		<header class="entry-header caption">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Lien permanent vers %s', 'hacking' ), the_title_attribute( 'echo=0' ) ) ); ?>">
				<?php echo ( has_post_thumbnail() ) ? get_the_post_thumbnail($post->ID, 'thumbnail', array('title' => '', 'class' => 'img-responsive')) : '<img class="img-responsive" src="http://placehold.it/717x478" alt="" />'; ?>
			</a>
			<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Lien permanent vers %s', 'hacking' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>

			<?php if ( 'post' == get_post_type() ) : ?>
			<div class="entry-meta">
				<?php gmm_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary caption">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-summary caption">
			<?php the_excerpt(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'hacking' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
