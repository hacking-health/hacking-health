<?php
/**
 * The template for displaying posts pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package GMM's Bootstrap
 * @since GMM's Bootstrap 1.0
 */

$name = $wp_query->queried_object->name;

get_header(); ?>
	<div class="banner-event-news">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<div class="entry-content">

					<div class="img-banner archive-news-banner">
						<div class="container">
							<div class="text-center">
								<p class="title"><?php printf( __( 'All posts from %s', 'hacking' ), $name); ?></p>
							</div>
						</div>
					</div>
					<header class="entry-header">
						<h1 class="entry-title"><?php _e('All posts', 'hacking'); ?></h1>
					</header><!-- .entry-header -->
					
					<div class="container">
						<div class="row">
							<?php while ( have_posts() ) : the_post(); ?>

								<div class="col-md-4">
									<div class="item-news bottom-spacing">
										<?php echo get_the_post_thumbnail( get_the_id(), 'img-news', array('class' => 'img-responsive img-temoignage center-block') ); ?>
										<div class="info-news clearfix">
											<p class="date"><?php echo strip_tags(gmm_posted_on()); ?></p>
											<p class="title"><?php echo mb_substr(strip_tags(get_the_title()), 0, 100).'...'; ?></p>
											<a href="<?php echo get_the_permalink(); ?>" class="text-right more-info"><span class="icon-plus"></span></a>
										</div>
									</div>
								</div>

							<?php endwhile; // end of the loop. ?>
						</div>
					</div>
				</div>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->
	</div>

<?php get_footer(); ?>